module Main where

import ETTypes
import ETParser
import ETEvaluator
import ETREPL
import Control.Monad
import System.IO
import System.Environment

main :: IO ()
main = do args <- getArgs
          if null args then runRepl else runOne $ args
