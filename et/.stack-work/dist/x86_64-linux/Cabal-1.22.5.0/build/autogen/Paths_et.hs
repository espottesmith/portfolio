module Paths_et (
    version,
    getBinDir, getLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/home/ess/code/adventures-in-haskell/et/.stack-work/install/x86_64-linux/lts-6.7/7.10.3/bin"
libdir     = "/home/ess/code/adventures-in-haskell/et/.stack-work/install/x86_64-linux/lts-6.7/7.10.3/lib/x86_64-linux-ghc-7.10.3/et-0.1.0.0-1H3ZL6b9lAkDJakogF6tFO"
datadir    = "/home/ess/code/adventures-in-haskell/et/.stack-work/install/x86_64-linux/lts-6.7/7.10.3/share/x86_64-linux-ghc-7.10.3/et-0.1.0.0"
libexecdir = "/home/ess/code/adventures-in-haskell/et/.stack-work/install/x86_64-linux/lts-6.7/7.10.3/libexec"
sysconfdir = "/home/ess/code/adventures-in-haskell/et/.stack-work/install/x86_64-linux/lts-6.7/7.10.3/etc"

getBinDir, getLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "et_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "et_libdir") (\_ -> return libdir)
getDataDir = catchIO (getEnv "et_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "et_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "et_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
