module ETTypes (ETValue(..), ETStatement(..), ETError(..), ThrowError, ThrowErrorIO, Env) where

import Text.ParserCombinators.Parsec
import Data.List
import Data.IORef
import System.IO
import Control.Monad
import Control.Monad.Except
import Control.Monad.Trans.Except

type ThrowErrorIO = ExceptT ETError IO
type Env = IORef [(String, IORef ETValue)]
type ThrowError = Either ETError
data ETValue = Symbol String | Bool Bool | Number Float | String String | Operator String ([ETValue] -> ThrowError ETValue) | Expression [ETValue] | List [ETValue]
data ETStatement = Assignment String ETValue | ITE ETValue ETStatement ETStatement | While ETValue ETStatement | Sequence [ETStatement]
data ETError = TypeMismatch String ETValue | Parser ParseError | NumArg Int [ETValue] | Nonexvar String | General String

instance Show ETError where show = showETError
instance Show ETValue where show = showETValue
instance Show ETStatement where show = showETStatement

showETValue :: ETValue -> String
showETValue (Bool val) = case val of
                            True -> "T"
                            False -> "F"
showETValue (Symbol val) = val
showETValue (Number val) = show val
showETValue (String val) = "\"" ++ val ++ "\""
showETValue (Operator val function) = val
showETValue (Expression val) = "("++ (mapExpression val) ++")"
showETValue (List val) = "/" ++ (mapList val) ++ "\\"

mapExpression :: [ETValue] -> String
mapExpression imput = unwords (map showETValue imput)

mapList :: [ETValue] -> String
mapList imput = intercalate ", " (map showETValue imput)


showETStatement :: ETStatement -> String
showETStatement (Assignment name value) = ":) " ++ name ++ " " ++ show value
showETStatement (ITE condition thenS elseS) = "if " ++ (show condition) ++ " then " ++ (showETStatement thenS) ++ " else " ++ (showETStatement elseS)
showETStatement (While condition something) = "while " ++ (show condition) ++ " do " ++ (showETStatement something)
showETStatement (Sequence statements) = intercalate "!\n" (map showETStatement statements)

showETError :: ETError -> String
showETError ( TypeMismatch typ arg) = "Type " ++ typ ++ " does not match " ++ show arg
showETError ( Parser parseError) = show parseError
showETError ( NumArg expected received ) = "We expected " ++ show expected ++ " but we received " ++ show received
showETError ( Nonexvar varname) = varname ++ " does not exsist "
showETError ( General message) = "You're a dingo because " ++ message
