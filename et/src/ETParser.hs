module ETParser (parseStatement, parseValue, spaces1) where

import ETTypes
import Text.ParserCombinators.Parsec
import Numeric
import Control.Monad

symbolChar :: Parser Char
symbolChar = oneOf "*-+/;:$%^<>&=`~|[]_"

spaces1 :: Parser ()
spaces1 = skipMany1 (char ' ')

parseBool :: Parser ETValue
parseBool = do
    character <- oneOf "TF"
    case character of
        'T' -> return (Bool True)
        'F' -> return (Bool False)

parseString :: Parser ETValue
parseString = do
    char '"'
    contents <- many (noneOf "\"")
    char '"'
    return (String contents)

parseOperator :: Parser ETValue
parseOperator = do
    operators <- many1 symbolChar
    return (Operator operators (\etValues -> return (head etValues)))

parseSymbol :: Parser ETValue
parseSymbol = do
    string <- many1 lower
    return (Symbol string)

parseInt :: Parser ETValue
parseInt = liftM (Number . read) $ many1 digit

parseFloat :: Parser ETValue
parseFloat = do
    begining <- many1 digit
    period <- char '.'
    end <- many1 digit
    (return . Number . fst . head . readFloat) (begining ++ [period] ++ end)

parsenumber :: Parser ETValue
parsenumber = try parseFloat <|> parseInt


parseValue :: Parser ETValue
parseValue = parseString <|> parseBool <|> parseSymbol <|> parsenumber <|> try parseList <|> parseOperator <|> parseExpression

parseList :: Parser ETValue
parseList = do
    char '/'
    ffuts <- sepBy parseValue (string ", ")
    char '\\'
    return (List ffuts)

parseExpression :: Parser ETValue
parseExpression = do
    char '('
    ffuts <- sepBy parseValue spaces1
    char ')'
    return (Expression ffuts)

parseAssignment :: Parser ETStatement
parseAssignment = do
    string ":)"
    spaces1
    name <- many1 lower
    spaces1
    value <- parseValue
    return (Assignment name value)

parseITE :: Parser ETStatement
parseITE = do
    string "if"
    spaces1
    condition <- parseValue
    spaces1
    string "then"
    spaces1
    thenStatment <- parseStatement
    spaces1
    string "else"
    spaces1
    elseStatement <-  parseStatement
    return (ITE condition thenStatment elseStatement)

parseWhile :: Parser ETStatement
parseWhile = do
    string "while"
    spaces1
    condition <- parseValue
    spaces1
    string "do"
    spaces1
    something <- parseStatement
    return (While condition something)

parseSequence :: Parser ETStatement
parseSequence = do
    char '{'
    statements <- sepBy parseStatement (string "! " <|> string "!\n")
    char '}'
    return (Sequence statements)

parseStatement :: Parser ETStatement
parseStatement = parseAssignment <|> parseITE <|> parseWhile <|> parseSequence
