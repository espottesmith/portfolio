module ETREPL (
                evalAndPrint
              , runOne
              , runRepl
              ) where

import System.IO
import Control.Monad
import Data.List

import ETTypes
import ETParser
import ETEvaluator

flushStr :: String -> IO ()
flushStr str = putStr str >> hFlush stdout

readPrompt :: String -> IO String
readPrompt prompt = flushStr prompt >> getLine

evalString :: Env -> String -> IO String
evalString env statement = runIOThrows $ liftM show $ (liftThrows $ readStatement statement) >>= eval env

evalAndPrint :: Env -> String -> IO ()
evalAndPrint env statement = evalString env statement >>= putStrLn

until_ :: Monad m => (a -> Bool) -> m a -> (a -> m ()) -> m ()
until_ pred prompt action = do
    result <- prompt
    if pred result
        then return ()
        else action result >> until_ pred prompt action

load :: String -> IOThrowsError [LispVal]
load filename = (liftIO $ readFile filename) >>= liftThrows . readExprList

loadEval :: Env -> String -> IOThrowsError ETValue
loadEval env filename = load filename >>= liftM last . mapM (evalStatement env)

runOne :: [String] -> IO ()
runOne args = do
    env <- primOp >>= flip bindVars [("args", List $ map String $ drop 1 args)]
    (runIOThrows $ liftM show $ loadEval env (args !! 0)) >>= hPutStrLn stderr

runRepl :: IO ()
runRepl = primOp >>= until_ (== "quit") (readPrompt "ET>>> ") . evalAndPrint
