module ETEvaluator (
                     readStatement
                   , extractValue
                   , trapError
                   , evalValue
                   , evalStatement
                   , runIOThrows
                   , liftThrows
                   , nullEnv
                   , primOP
                   , bindVars
                   ) where

import ETTypes
import ETParser
import Control.Monad.Except
import Control.Monad.Trans.Except
import Data.List
import Data.IORef
import System.IO
import Control.Monad
import Control.Monad.IO.Class
import Text.ParserCombinators.Parsec hiding (spaces)

evalValue :: Env -> ETValue -> ThrowErrorIO ETValue
evalValue env (Symbol name) = getVar env name
evalValue env val@(String _) = return val
evalValue env val@(Number _) = return val
evalValue env val@(Bool _) = return val
evalValue env (Operator name func) = return (Operator name (getFunc name))
evalValue env (Expression contents) = do
    evalContents <- (mapM (evalValue env) contents)
    apply (head evalContents ) (tail evalContents)

evalStatement :: Env -> ETStatement -> ThrowErrorIO ETValue
evalStatement env (Assignment name val) = evalValue env val >>= defineVar env name
evalStatement env (ITE condition thenS elseS) = if (truth env condition)
                                                    then evalStatement env thenS
                                                    else evalStatement env elseS
evalStatement env (While condition statements) = if (truth env condition)
                                                    then (evalStatement env statements >> evalStatement env (While condition statements))
                                                    else return (Bool False)
evalStatement env (Sequence statements) = mapM (evalStatement env) statements >>= return . last

truth :: Env -> ETValue -> Bool
truth env (Bool b) = b
truth env (String "") = False
truth env (String _) = True
truth env (Number 0.0) = False
truth env (Number _) = True
truth env (List []) = False
truth env (List _) = True
truth env (Symbol name) = if isBound env name then True else False 
truth env other = return $ TypeMismatch "bool, num, str, list, or expression" other

apply :: ETValue -> [ETValue] -> ThrowErrorIO ETValue
apply (Operator _ function) args = liftThrows $ function args

primOp :: [(String,([ETValue] -> ThrowError ETValue))]
primOp = [("+",plus),
		  ("-",minus),
		  ("/",division),
		  ("*",multiplication)
		  ("**",etExponent),
		  ("~",etNot),
		  ("=",equal),
		  ("<",lessThan),
		  (">",greaterThan),
		  ("&",etAnd),
		  ("<>",etOr),
		  ("===",makeString),
		  ("+=",concatenateString),
		  ("<-->",stringReverse),
		  ("??",indexOp)]

getFunc :: String -> ([ETValue] -> ThrowError ETValue)
getFunc name = case elemIndex name (map fst primOp) of
                    Just index -> snd (primOp !! index)
                    Nothing -> head

plus :: [ETValue] -> ThrowError ETValue
plus [( Number num1), (Number num2)] = return (Number (num1 + num2))
plus args =  return (NumArg 2 args)

minus :: [ETValue] -> ThrowError ETValue
minus [(Number num1), (Number num2)] = return (Number (num1 - num2))
minus args = return (NumArg 2 args)

division :: [ETValue] -> ThrowError ETValue
division [(Number num1), (Number num2)] = return (Number num1 / num2)
division args = return (NumArg 2 args)

multiplication :: [ETValue] -> ThrowError ETValue
multiplication [(Number num1), (Number num2)] = return (Number num1 * Number num2)
multiplication args = return (NumArg 2 args)

etExponent :: [ETValue] -> ThrowError ETValue
etExponent  [(Number num1), (Number num2)] = return (Number (num1 ** num2))
etExponent args = return (NumArg 2 args)

etNot :: [ETValue] -> ThrowError ETValue
etNot [(Bool arg)] = return (Bool (not arg))
etNot args = return (NumArg 1 args)

lessThan :: [ETValue] -> ThrowError ETValue
lessThan [(Number num1), (Number num2)] = if num2 < num2 then return (Bool True) else return (Bool False)
lessThan args = return (NumArg 2 args)

greaterThan :: [ETValue] -> ThrowError ETValue
greaterThan [(Number num1), (Number num2)] = if num1 > num2 then return (Bool True) else return (Bool False)
greaterThan args = return (NumArg 2 args)

etAnd :: [ETValue] -> ThrowError ETValue
etAnd [(Bool arg1), (Bool arg2)] = if arg1 && arg2 then return (Bool True) else return (Bool False)
etAnd args = return (NumArg 2 args)

etOr :: [ETValue] -> ThrowError ETValue
etOr [(Bool arg1), (Bool arg2)] = if arg1 || arg2 then return (Bool True) else return (Bool False)
etOr args = return (NumArg 2 args)

makeString :: [ETValue] -> ThrowError ETValue
makeString [arg] = return (show arg)
makeString args = return (NumArg 1 args)

concatenateString :: [ETValue] -> ThrowError ETValue
concatenateString [(String str1), (String str2)] = return (String (str1 ++ str2))
concatenateString args = return (NumArg 2 args)

stringReverse :: [ETValue] -> ThrowError ETValue
stringReverse [(String arg)] = return (String (reverse arg))
stringReverse args = return (NumArg 1 args)

indexOp :: [ETValue] -> ThrowError ETValue
indexOp [(List list), (Number num)] = return (list !! (floor num))
indexOp args = return (NumArg 2 args)

equal :: [ETValue] -> ThrowError ETValue
equal [l1@(List arg1), l2@(List arg2)] = eqvList equal [l1, l2]
equal [(String arg1), (String arg2)] = if arg1 == arg2 then return (Bool True) else return (Bool False)
equal [(Number arg1), (Number arg2)] = if arg1 == arg2 then return (Bool True) else return (Bool False)
equal [(Bool arg1), (Bool arg2)] = if arg1 == arg2 then return (Bool True) else return (Bool False)
equal [e1@(Expression _), e2@(Expression _)] = equal [evalValue e1, evalValue e2]
equal [e1@(Expression _), arg2] = equal [evalValue e1, arg2]
equal [arg1, e2@(Expression _)] = equal [arg1, evalValue e2]
equal [arg1, arg2] = return (TypeMismatch "Equivalent types" [arg1, arg2])
equal args = return (NumArg 2 args)

eqvList :: ([ETValue] -> ThrowError ETValue) -> [ETValue] -> ThrowError ETValue
eqvList eqvFunc [(List arg1), (List arg2)] = return $ Bool $ (length arg1 == length arg2) &&
                                                             (all eqvPair $ zip arg1 arg2)
    where eqvPair (x1, x2) = case eqvFunc [x1, x2] of
                                  Left err -> False
                                  Right (Bool val) -> val

primitiveBindings :: IO Env
primitiveBindings = nullEnv >>= (flip bindVars $ map (makeFunc Operator) primOp)
    where makeFunc constructor (var, func) = (var, constructor var func)

isBound :: Env -> String -> IO Bool
isBound envRef var = readIORef envRef >>= return . maybe False (const True) . lookup var

getVar :: Env -> String -> ThrowErrorIO ETValue
getVar envRef var = do env <- liftIO $ readIORef envRef
                       maybe (trapError $ Nonexvar "Getting an unbound variable" var)
                             (liftIO . readIORef)
                             (lookup var env)

setVar :: Env -> String -> ETValue -> ThrowErrorIO ETValue
setVar envRef var value = do env <- liftIO $ readIORef envRef
                             maybe (throwError $ Nonexvar "Setting an unbound variable" var)
                                   (liftIO . (flip writeIORef value))
                                   (lookup var env)
                             return value

defineVar :: Env -> String -> ETValue -> ThrowErrorIO ETValue
defineVar envRef var value = do
     alreadyDefined <- liftIO $ isBound envRef var
     if alreadyDefined
         then setVar envRef var value >> return value
         else liftIO $ do
             valueRef <- newIORef value
             env <- readIORef envRef
             writeIORef envRef ((var, valueRef):env)
             return value

bindVars :: Env -> [(String, ETValue)] -> IO Env
bindVars envRef bindings = readIORef envRef >>= extendRef bindings >>= newIORef
     where extendRef bindings env = liftM (++ env) (mapM addBinding bindings)
           addBinding (var, value) = do ref <- newIORef value
                                        return (var, ref)

nullEnv :: IO Env
nullEnv = newIORef []

trapError action = catchError action (return . show)

liftThrows :: ThrowError a -> ThrowErrorIO a
liftThrows (Left err)  = throwError err
liftThrows (Right val) = return val

runIOThrows :: ThrowErrorIO String -> IO String
runIOThrows action = runExceptT (trapError action) >>= return . extractValue

extractValue :: ThrowError a -> a
extractValue (Right val) = val

readOrThrow :: Parser a -> String -> ThrowError a
readOrThrow parser input = case parse parser "et" input of
    Left err  -> throwError $ Parser err
    Right val -> return val

readStatement = readOrThrow parseStatement
readStatementList = readOrThrow (endBy parseStatement spaces1)
