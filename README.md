# Evan Spotte-Smith


## Introduction ##

Included in this portfolio are a number of projects that I have worked on during my time as a high school student and college student. Most projects included source code (Python, Haskell, and C++). However, also included are some CAD files, as well as a binary executable file and a PDF (in Downloads). This README will describe each of the projects in the portfolio, in order to provide a context for my work. Projects are listed in chronological order.


## Projects ##


1. 2014-controllers/

    During the 2013-2014 academic year (during which time I was in my Junior year of high school), I participated in the Maryland MESA competition, a group of math, science, and engineering challenges. Specifically, I worked on the Cyber Robot Challenge, in which students had to program a digital "robot" to navigate a maze, collecting points by reaching "viruses" before eventually finding an exit to the maze level. The controllers in 2014-controllers are a mix of required controllers (in controllers/) and user-defined algorithms (in algorithms/). My main work on this project was in implementing Depth-First Search and a General Greedy Best-First Search, although I contributed to nearly every algorithm.

2. Multi-Agent System (MAS) Simulator

    During my 2014-2015 internship at the Johns Hopkins University Applied Physics Laboratory, I worked on developing cooperation algorithms for MAS artificial intelligence in C++. In MAS, a number of independent intelligent entities interact in an environment to achieve goals. Specifically, for my work, the agents all cooperated to seek and destroy targets in an open environment. Because the source code is proprietary and owned by the Applied Physics Laboratory, I cannot include source code, but I have included in Downloads an executable of the project, as well as a research paper I wrote on my work on cooperation algorithms. Note that the executable requires an Ubuntu-based operating system and specifically requires libGLEW version 1.10.

3. coursework/engi1006/

    Included in this directory are all homework and project assignments for the Introduction to Computing for Engineers and Applied Scientists course at Columbia University.

4. coursework/engi1102/

    Included this directory is the final project for The Art of Engineering course at Columbia University. For this course, students had to design a remote control specifically for senior citizens and 3D print it.

5. cad/

    While I work regularly with CAD software in my role as a machine operator, I rarely get the opportunity to produce projects of my own. These files are the exception, a series of bracelets designed as a graduate present for a friend of mine.

6. et/

    When I worked as a programming counselor during the summer of 2016, two of my campers decided that they wanted to design their own programming language, ET. After I described Haskell to the campers, they further decided that they wanted to write ET in Haskell. The result is ET, a simple imperative language with a parser, evaluator, and REPL. I worked on ET alongside the campers, although most code was written or planned by me before I worked through it with the campers. Note that ET will not compile; while the campers were able to finish their project, they did not have sufficient time to remove all errors.
