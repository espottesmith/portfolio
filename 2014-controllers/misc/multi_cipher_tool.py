# multi_cipher_tool.py
# Cipher-Solver
#
# This module contains the capacity to quickly solve ciphers seen in the maze.
#     It operates via interactivity - it should be used in environments such as
#     python shells.
#
#
# Written by: Eric Sturm during MESA 2012 (Cipher, Vigenere, Transposition)
#   Marcus Fedarko during MESA 2012/2014 (Public/Private Key, Polybius Square)

import string

def sanitize(input_text):
    """returns a modified version of the input text, converted to uppercase
    and stripped of punctuation and whitespace.

    inputs:
    input_text: the text to be sanitized

    outputs:
    output_text: the finalized text

    """

    output_text = input_text.translate(None, string.punctuation)
    output_text = output_text.replace(" ", "")
    output_text = output_text.upper()    

    return output_text

again = 'Y'

while (again[0] == 'Y' or again[0] == 'y'):
    # (Re-)initialization
    EorD = ''
    in_text = ''
    out_text = ''
    fixed_text = ''
    keyword = ''
    fixed_key = ''
    shift = 0
    op = -1
    Base = ord('A')

    # Setup
    # Find out what we need to do and what the input stream is
    EorD = raw_input("Is the text to follow to be E(ncoded) or D(ecoded)? ")
    in_text = raw_input("What is this input text? ")
    print "\nWhat type of cipher are we working with?"
    cipher = \
        raw_input("C(aesar), V(igenere), T(ransposition), PU(blic/Private Key), PO(lybius Square) ")

    if EorD[0] == 'E' or EorD[0] == 'e':
        op = 1

    # Pre-Processing
    # Converting all text to upper case and stripping out all punctuation
    fixed_text = sanitize(in_text)

    print fixed_text
    
    # Caesar Engine
    if cipher[0] == 'C' or cipher[0] == 'c':
        shift = int(raw_input("\nWhat is the shift? "))
        for i in tuple(fixed_text):
            if i != " ":
                init_char = ord(i) + (op * shift)
                fin_char = ((init_char - 65) % 26) + 65
                out_text = out_text + chr(fin_char)

    # Vigenere Engine
    elif cipher[0] == 'V' or cipher[0] == 'v':
        keyword = raw_input("\nWhat is the keyword? ")
        fixed_key = sanitize(keyword)

        keyword = fixed_key
        cycles = ((len(fixed_text) // len(keyword)) + 1)
        k_len = len(keyword)
        print(cycles, k_len)

        for i in range(cycles):
            for j in range(k_len):
                try:
                    if (fixed_text[(i*k_len) + j] != " "):
                        init_char = ord(fixed_text[(i*k_len) + j]) + (op * (ord(keyword[j]) - 64))
                        fin_char = ((init_char - 65) % 26) + 65
                        out_text += chr(fin_char)
                        print(i, j, fixed_text[(i*k_len) + j], keyword[j], init_char, fin_char)
                except:
                    pass

    # Transposition Engine
    elif cipher[0] == 'T' or cipher[0] == 't':
        keyword = raw_input("What is the keyword? ")
        # Make keyword all upper case
        fixed_key = sanitize(keyword)

        keyword = fixed_key
        cycles = int(round(len(fixed_text) / len(keyword)))
        k_len = len(keyword)
        in_dict = {}
        out_dict = {}
        mid_text = ''

        # Encryption
        if op == 1:
            for i in range(k_len):
                in_dict[keyword[i]] = fixed_text[i::k_len]
                print (in_dict)

            for i in tuple(sorted(keyword)):
                out_text += in_dict[i]

        # Decryption
        elif op == -1:
            j = 0
            for i in tuple(sorted(keyword)):
                in_dict[i] = fixed_text[(j * cycles):((j*cycles) + cycles)]
                j += 1
                print (in_dict)

            for i in tuple(keyword):
                mid_text += in_dict[i]

            j = 0
            for i in range(cycles):
                for j in range(k_len):
                    out_text += mid_text[(j * cycles) + i]

        else:
            print (op)
            print ("There was an error in the processing code")

    # Public Key Engine
    elif (cipher[0] == 'P' or cipher[0] == 'p') and \
        (cipher[1] == 'U' or cipher[1] == 'u'):
        
        in_text_num = -1
        try:
            in_text_num = int(in_text)
        except ValueError:
            print "ERROR: Input text for Public/Private Key Cipher must be an integer"
            print "Starting over...\n"
            continue

        # Encryption
        if op == 1:
            print "===ENCODING==="
            print "Public: (n, e)"
            print "Private: (n, d)"
            n = input("n > ")
            e = input("e > ")
            d = input("d > ")

            out_text = (e * in_text_num) % n
            print out_text
            
        # Decryption
        if op == -1:
            print "===DECODING==="
            print "Public: (n, e)"
            print "Private: (n, d)"
            n = input("n > ")
            e = input("e > ")
            d = input("d > ")

            out_text = (d * in_text_num) % n
            print out_text

    #Polybius Square Engine
    elif (cipher[0] == 'P' or cipher[0] == 'p') and \
        (cipher[1] == 'O' or cipher[1] == 'o'):
        
        print "Assembling a table of format:"
        print "_|X_K_E_Y_1"
        print "Y|A B C D E"
        print "K|F G H I J"
        print "E|K L M N O"
        print "Y|P Q R S T"
        print "1|U V W X Y"

        xkey = raw_input("XKEY1, from left to right > ") 
        ykey = raw_input("YKEY1, from top to bottom > ")
        row1 = raw_input("First row of non-key letters > ")
        row2 = raw_input("Second row of non-key letters > ")
        row3 = raw_input("Third row of non-key letters > ")
        row4 = raw_input("Fourth row of non-key letters > ")
        row5 = raw_input("Fifth (final) row of non-key letters > ")

        # Encryption
        if op == 1:
            print "===ENCODING==="
            for c in fixed_text:
                if c in row1:
                    out_text += ykey[0]
                    out_text += xkey[row1.index(c)]
                elif c in row2:
                    out_text += ykey[1]
                    out_text += xkey[row2.index(c)]
                elif c in row3:
                    out_text += ykey[2]
                    out_text += xkey[row3.index(c)]
                elif c in row4:
                    out_text += ykey[3]
                    out_text += xkey[row4.index(c)]
                elif c in row5:
                    out_text += ykey[4]
                    out_text += xkey[row5.index(c)]
                else:
                    print "Could not locate letter \"" + c + "\" in table."
                    print "Starting over...\n"
                    continue

        # Decryption
        if op == -1:
            print "===DECODING==="
            # break ciphertext into chunks of 2 characters for row/column
            # all ciphertext for this should have an even number of characters
            # (n * 2 is even for all n)
            chunk_list = []
            chunk = ""
            for c in fixed_text:
                chunk += c
                if len(chunk) == 2:
                    chunk_list.append(chunk)
                    chunk = ""
            print chunk_list
            
            # now, interpret each chunk as an encoded letter
            for ch in chunk_list:
                y_index = ykey.index(ch[0])
                x_index = xkey.index(ch[1])
                if y_index == 0:
                    out_text += row1[x_index]
                elif y_index == 1:
                    out_text += row2[x_index]
                elif y_index == 2:
                    out_text += row3[x_index]
                elif y_index == 3:
                    out_text += row4[x_index]
                elif y_index == 4:
                    out_text += row5[x_index]
                else:
                    print "Encountered an error decoding the input."
                    print "Starting over...\n"
                    continue


    # Print Results
    print in_text
    print fixed_text
    print out_text

    again = raw_input("\nDo you have more text to encrypt or decrypt? (Y/N) ")
