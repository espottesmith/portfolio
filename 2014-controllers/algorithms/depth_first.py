# depth_first.py
# Depth-First Search Algorithm
# 
# Implements Depth-first Search (DFS) in order to guarantee that the robot
#	locates everything.
# DFS is not a shortest-path algorithm, but will find all viruses and exits,
#	as it searches every node in the graph. This DFS implementation is modified,
#	including backtracking and targeted movement towards exit.
# 
#
# Written by: Evan Spotte-Smith and Tomi Okiji
# Edited by: Kai Atanasoff and Marcus Fedarko

def control_robot(robot):
	"""implements Depth-First Search (DFS) with backtracking.

	inputs:
	robot: The robot object.

	outputs: None

	"""

	global all_nodes, current_dir, current_node
	global FORWARD, RIGHT, BACKWARD, LEFT
        
	FORWARD = 0
	RIGHT = 1
	BACKWARD = 2
	LEFT = 3


	def make_node(pos, parent=None, visited=False, exit=False):
		"""creates a dict shell for a objective-coordinate node.

		inputs:
		pos: 2-tuple. The position that this node holds in the graph
		parent: 2-tuple. The position of the node previously accessed
		visited: Boolean.
		exit: Boolean. True only if robot.sense_exits() returns this node's pos

		outputs:
		node: A dict with position, parent, and visited values

		"""
        
		global all_nodes, current_dir, current_node
		global FORWARD, RIGHT, BACKWARD, LEFT

		return {
			'pos':pos,
			'parent':parent,
			'visited':visited,
			'exit':exit
		}


	def find_node(direction=None, point=None):
		"""determine if a particular node exists, returns node if possible.

		inputs:
		direction: direction. int in range(0,4)
		point: a 2-tuple defining coordinates. By default, this attribute is
		set to None, meaning that find_node() will use whatever square is
		one ahead of the robot as the Node to search for.
		If point is specified, find_node() will search for a node with a
		pos attribute equal to point.

		outputs:
		other_node: node. The desired node dict.
		None: if point is not specified and there is no square one ahead of
		the robot, None is returned. This is useful for conditional statements
		in other methods and functions (see: move()).

		"""

		global all_nodes, current_dir, current_node
		global FORWARD, RIGHT, BACKWARD, LEFT

		pos = current_node['pos']


		if point != None:		# A specific point is given (exit, virus, etc.)
			node_found = False
			for other_node in all_nodes:
				if other_node['pos'] == point:
					node_found = True
					return other_node

			if not node_found:
				node_there = make_node(point)
				all_nodes.append(node_there)
				return node_there

		elif direction != None:	# A specific direction to check is given
			if direction == current_dir:
				if robot.sense_steps(robot.SENSOR_FORWARD) > 0:
					if current_dir == FORWARD:
						node_found = False
						for other_node in all_nodes:
							if other_node['pos'] == (pos[0], pos[1] + 1):
								node_found = True
								return other_node
						
						if not node_found:
							node_there = make_node((pos[0], pos[1] + 1))
							all_nodes.append(node_there)
							return node_there

					elif current_dir == RIGHT:
						node_found = False
						for other_node in all_nodes:
							if other_node['pos'] == (pos[0] + 1, pos[1]):
								node_found = True
								return other_node
						
						if not node_found:
							node_there = make_node((pos[0] + 1, pos[1]))
							all_nodes.append(node_there)
							return node_there

					elif current_dir == BACKWARD:
						node_found = False
						for other_node in all_nodes:
							if other_node['pos'] == (pos[0], pos[1] - 1):
								node_found = True
								return other_node
						
						if not node_found:
							node_there = make_node((pos[0], pos[1] - 1))
							all_nodes.append(node_there)
							return node_there

					elif current_dir == LEFT:
						node_found = False
						for other_node in all_nodes:
							if other_node['pos'] == (pos[0] - 1, pos[1]):
								node_found = True
								return other_node

						if not node_found:
							node_there = make_node((pos[0] - 1, pos[1]))
							all_nodes.append(node_there)
							return node_there
				
				else:
					return None

			elif direction == (current_dir + 1) % 4:
				if robot.sense_steps(robot.SENSOR_RIGHT) > 0:
					if current_dir == FORWARD:
						node_found = False
						for other_node in all_nodes:
							if other_node['pos'] == (pos[0] + 1, pos[1]):
								node_found = True
								return other_node
						
						if not node_found:
							node_there = make_node((pos[0] + 1, pos[1]))
							all_nodes.append(node_there)
							return node_there

					elif current_dir == RIGHT:
						node_found = False
						for other_node in all_nodes:
							if other_node['pos'] == (pos[0], pos[1] - 1):
								node_found = True
								return other_node
						
						if not node_found:
							node_there = make_node((pos[0], pos[1] - 1))
							all_nodes.append(node_there)
							return node_there

					elif current_dir == BACKWARD:
						node_found = False
						for other_node in all_nodes:
							if other_node['pos'] == (pos[0] - 1, pos[1]):
								node_found = True
								return other_node
						
						if not node_found:
							node_there = make_node((pos[0] - 1, pos[1]))
							all_nodes.append(node_there)
							return node_there

					elif current_dir == LEFT:
						node_found = False
						for other_node in all_nodes:
							if other_node['pos'] == (pos[0], pos[1] + 1):
								node_found = True
								return other_node

						if not node_found:
							node_there = make_node((pos[0], pos[1] + 1))
							all_nodes.append(node_there)
							return node_there
				else:
					return None

			elif direction == (current_dir - 1) % 4:
				if robot.sense_steps(robot.SENSOR_LEFT) > 0:
					if current_dir == FORWARD:
						node_found = False
						for other_node in all_nodes:
							if other_node['pos'] == (pos[0] - 1, pos[1]):
								node_found = True
								return other_node
						
						if not node_found:
							node_there = make_node((pos[0] - 1, pos[1]))
							all_nodes.append(node_there)
							return node_there

					elif current_dir == RIGHT:
						node_found = False
						for other_node in all_nodes:
							if other_node['pos'] == (pos[0], pos[1] + 1):
								node_found = True
								return other_node
						
						if not node_found:
							node_there = make_node((pos[0], pos[1] + 1))
							all_nodes.append(node_there)
							return node_there

					elif current_dir == BACKWARD:
						node_found = False
						for other_node in all_nodes:
							if other_node['pos'] == (pos[0] + 1, pos[1]):
								node_found = True
								return other_node
						
						if not node_found:
							node_there = make_node((pos[0] + 1, pos[1]))
							all_nodes.append(node_there)
							return node_there

					elif current_dir == LEFT:
						node_found = False
						for other_node in all_nodes:
							if other_node['pos'] == (pos[0], pos[1] - 1):
								node_found = True
								return other_node

						if not node_found:
							node_there = make_node((pos[0], pos[1] - 1))
							all_nodes.append(node_there)
							return node_there

				else:
					return None
		else:
			return None 	# Nothing specified


	def change_dir(direction):
		"""change robot orientation while maintaining current_dir.
		
		inputs:
		direction: int in range(0,4) - FORWARD, RIGHT, BACKWARD, or LEFT.
		
		outputs: None
		
		direction is first sanity-tested. If it passes and the robot's current
		direction is alreads accurate, nothing happens. Otherwise, the
		current_dir variable is changed to ensure that current_dir
		is still in range(0,4), and then the robot is reoriented with
		robot.turn_left or robot.turn_right, depending on whether 
		direction > current_dir or direction < current_dir.
		
		"""

		global all_nodes, current_dir, current_node
		global FORWARD, RIGHT, BACKWARD, LEFT
		
		if direction in range(0,4):
			if current_dir == direction:
				pass
			elif current_dir < direction:
				difference = direction - current_dir
				robot.turn_right(difference)
				current_dir += difference
			elif current_dir > direction:
				difference = current_dir - direction
				robot.turn_left(difference)
				current_dir -= difference
		else:
			raise ValueError("direction must be FORWARD (0), "
				"RIGHT (1), BACKWARD (2), or LEFT (3)")


	def find_dir(node):
		"""return the direction of movement required to reach a point.

		inputs:
		node: dict. containing the coordinates of a point

		outputs:
		direction: either FORWARD, BACKWARD, LEFT, or RIGHT

		This function is an intermediary which will reduce code reuse for
		determining how to proceed and how to move in the graph.

		"""

		global all_nodes, current_dir, current_node
		global FORWARD, RIGHT, BACKWARD, LEFT

		x = current_node['pos'][0]
		y = current_node['pos'][1]

		if node['pos'] == (x, y + 1):
			return FORWARD
		elif node['pos'] == (x + 1, y):
			return RIGHT
		elif node['pos'] == (x, y - 1):
			return BACKWARD
		elif node['pos'] == (x - 1, y):
			return LEFT


	def move(direction, num_steps=1, backtracking=False):
		"""wrapper function for robot.step_forward().
		
		inputs: 
		direction: int in range(0,4)
			Must equal either FORWARD, LEFT, BACKWARD, or RIGHT
		num_steps: int, as in robot.step_forward()
		backtracking: boolean.
		
		outputs: None
		
		Robot orients itself to face direction, then moves one step at a time,
		maintaining current position and a list of all nodes as it goes.

		"""
		
		global all_nodes, current_dir, current_node
		global FORWARD, RIGHT, BACKWARD, LEFT

		change_dir(direction)
		
		steps_taken = 0
		while steps_taken < num_steps:
			next_step = find_node(direction=current_dir)
			if next_step != None:
				robot.step_forward()

				if not backtracking:
					next_step['parent'] = current_node['pos']
				
				if not next_step['visited']:
					next_step['visited'] = True

				current_node = next_step

			steps_taken += 1


	def initialize():
		"""establishes the graph for the algorithm to traverse.

		inputs: None
		outputs: None

		"""

		global all_nodes, current_dir, current_node
		global FORWARD, RIGHT, BACKWARD, LEFT
		
		current_dir = FORWARD

		current_node = make_node((0,0), visited=True)
		all_nodes = [current_node]


		for pos in robot.sense_exits():
		 	all_nodes.append(make_node(tuple(pos), exit=True))


	def check_sides(): 
		"""checks sides to determine if backtracking is necessary.
		
		inputs: None

		outputs:
		next_dir: direction (int in range(0,4)) or None
		next_node: node dict, or None

		"""

		global all_nodes, current_dir, current_node
		global FORWARD, RIGHT, BACKWARD, LEFT

		pos = current_node['pos']

		forward_node = find_node(direction=current_dir)
		right_node = find_node(direction=(current_dir + 1)%4)
		left_node = find_node(direction=(current_dir - 1)%4)


		# to iterate through the robot's neighbors, essentially.
		node_positions = [forward_node, right_node, left_node]

		for node in node_positions:
			if node != None:
				if not node['visited']:
					return find_dir(node), node

		# There are no unvisited nodes adjacent to this node.
		return None, None


	def backtrack(next_node):
		"""moves towards start until unvisited nodes appear.

		inputs:
		next_node: node dict, or None

		outputs: None

		direction will most likely come from check_sides[1]

		"""

		global all_nodes, current_dir, current_node
		global FORWARD, RIGHT, BACKWARD, LEFT
		
		options = next_node
		while not options and not current_node['pos'] == (0,0):
			parent_node = find_node(point=current_node['parent'])
			move(find_dir(parent_node), backtracking=True)

			options = check_sides()[1]


	def extend(node):
		"""moves the robot to a particular node based on parent nodes.

		inputs:
		node - a node dict with a parent attribute

		outputs: None

		Creates a list of parent nodes, ending with the goal.
		Then, the robot moves along that path, thus bringing it to
		the input/target node.

		"""
		
		global all_nodes, current_dir, current_node
		global FORWARD, RIGHT, BACKWARD, LEFT
		
		move_list = [node]

		if node['parent']:
			next = node
			while next['parent']:
				if next['parent'] == current_node['pos']:
					# reverses move_list
					move_list = move_list[::-1]
					for n in move_list:
						move(find_dir(n))
				else:
					move_list.append(find_node(point=next['parent']))
				next = find_node(point=next['parent'])
			
		else:
			print "Error! Extending with a parentless node!"


	def move_to_location(next_dir, next_node):
		"""uses check_sides() to orient and direct robot.

		inputs:
		next_dir: direction (int in range(0,4)) or None
		next_node: node dict or None

		outputs: None
 
 		The inputs will most likely come from check_sides().
		
		"""

		global all_nodes, current_dir, current_node
		global FORWARD, RIGHT, BACKWARD, LEFT
	
		if next_node: # there are unvisited available nodes 
			move(next_dir)

		else:
			backtrack(next_node)


	initialize()

	direction, node = check_sides()
	# case handling: starting facing a dead end
	if not direction and not node:
		change_dir(BACKWARD)
		direction, node = check_sides()

	while True:
		move_to_location(direction, node)
		if current_node['pos'] == (0,0) and not node and \
			robot.num_viruses_left() == 0:
			# all nodes have been searched; time to find exit
			for node in all_nodes:
				if node['exit']:
					extend(node)
		direction, node = check_sides()
