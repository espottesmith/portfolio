# random_passages.py
# Random Mouse Algorithm (Passage-Focused)
#   also known as randomyolo.py
#
# In this algorithm, the robot acts like a mouse with no memory or reaction to
#   stimuli while attempting to solve a maze. The robo-mouse evaluates possible
#   directions before every passage and chooses a random direction via Python's
#   pseudo-random built-in random.choice() function.
# 
# Unlike its sibling algorithm, random_steps, this algorithm is not guaranteed
#   to solve a given maze perfectly, due to the possibility of "holes" in the
#   sides of passages that the robot may ignore. It's still useful for some
#   scenarios, though, and usually more time-efficient than random_steps.
# 
# Written by: Oh god, do I actually want to put my name on this...
# Okay, Kai Atanasoff. (and Marcus Fedarko)
# 
# Edited by: Evan Spotte-Smith
# 
# 
# Alright, guys... yolo.

from random import choice # harbinger of insanity part 2: electric boogaloo

def control_robot(robot):
    """implements Random Mouse (Random YOLO) "pathfinding algorithm". #yoloswag

    inputs:
    robot: the robot object

    outputs: None (more accurately, hilarity)

    Picks a random direction
    and moves there.

    Swag me out, Random YOLO.

    """
    
    # simple directional identifier constants
    AHEAD = "ahead"
    LEFT = "left"
    RIGHT = "right"
    
    while True:
        step_ahead = robot.sense_steps(robot.SENSOR_FORWARD)
        step_left = robot.sense_steps(robot.SENSOR_LEFT)
        step_right = robot.sense_steps(robot.SENSOR_RIGHT)
        
        # based on sensing results, calculate a list of viable directions
        # that the robot could go in
        
        opt_lengths = [step_ahead, step_left, step_right]
        potential_options = [AHEAD, LEFT, RIGHT]
        viable_options = []
        
        c = 0
        for opt in opt_lengths:
            if opt > 0:
                viable_options.append(potential_options[c])
            c += 1

        if len(viable_options) == 0:
            robot.turn_right(2)
            robot.step_forward()
        else:
            # Now she want a photo /
            # You already know though /
            # You only live once - that's the motto [...] YOLO
            # (You Only Locate [an activated exit to a maze] Once)
            the_one_choice = choice(viable_options)
            if the_one_choice == AHEAD:
                robot.step_forward(step_ahead)
            elif the_one_choice == LEFT:
                robot.turn_left(1)
                robot.step_forward(step_left)
            elif the_one_choice == RIGHT:
                robot.turn_right(1)
                robot.step_forward(step_right)