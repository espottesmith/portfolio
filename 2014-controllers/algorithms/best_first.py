# best_first.py
# Greedy Best-First Search
#
# Implements a basic Best-First Search to find all viruses and exits in
#	"reasonable" time.
# Best-First Searches move at each stage to the next most-promising option
#	In this case, at each node, the robot moves to the node with the lowest
#	taxicab distance to the goal in question that has not been visited
#	(if a node is visited and encountered again, there is a loop or the original
#	search failed).
# This algorithm is considered "greedy" because it only thinks ahead one move at
#	a time, rather than planning an actual ideal path.
#
#
# Written by: Evan Spotte-Smith
# Edited by: Marcus Fedarko

def control_robot(robot):
	"""implements Greedy Best-First Search.

	inputs: 
	robot: The robot object.

	outputs: None

	"""

	global all_nodes, current_dir, current_node, goal, targets
	global FORWARD, RIGHT, BACKWARD, LEFT
        
	FORWARD = 0
	RIGHT = 1
	BACKWARD = 2
	LEFT = 3


	def taxicab_dist(beginning, end):
		"""standard implementation of two-dimensional taxicab distance formula.

	    inputs:
	    beginning: 2-tuple. The start coordinates.
	    end: 2-tuple. The end coordinates.

	    outputs:
	    output of two-dimensional distance formula in taxicab geometry:
	        d = (x_2 - x_1) + (y_2 - y_1)
	    with x_1 and y_1 originating from this node's location.

	    In taxicab geometry, a discrete, non-continuous geometric system,
	    points are defined unconventionally: rather than having points at all
	    real numbers, there are only points at every integral value, ie -1, 0,
	    1, etc. As such, a straight like can no longer be defined as the
	    shortest distance between two points, as often, one cannot make a
	    straight line between two points (like between (0,0) and (1,1), in
	    which the optimal path either goes through (0,1) or through (1,0)).
	    This function was chosen, as opposed to the traditional distance
	    formula math.sqrt((x_2 - x_1)**2 + (y_2 - y_1)**2), because the robot
	    cannot move diagonally, but can only go FORWARD, RIGHT, BACKWARD, or 
	    LEFT.

	    ***NOTE: THIS IS THE SAME THING AS A MANHATTAN HEURISTIC FUNCTION.***

	    Does not necessarily produce the actual distance or cost from a start
	    point to the goal.

	    """

		return abs(end[0] - beginning[0]) + abs(end[1] - beginning[1])


	def make_node(pos, parent=None, visited=False, virus=False, exit=False):
		"""creates a dict shell for a objective-coordinate node.

		inputs:
		pos: 2-tuple. The position that this node holds in the graph

		outputs:
		node: A dict with position, parent, and visited values
		"""
        
		global all_nodes, current_dir, current_node, goal, targets
		global FORWARD, RIGHT, BACKWARD, LEFT

		return {
			'pos':pos,
			'parent':parent,
			'visited':visited,
			'virus':virus,
			'exit':exit,
			'dist':0
		}


	def find_node(direction=None, point=None):
		"""determine if a particular node exists, returns node if possible.

		inputs:
		direction: direction. int in range(0,4)
		point: a 2-tuple defining coordinates. By default, this attribute is
		set to None, meaning that find_node() will use whatever square is
		one ahead of the robot as the Node to search for.
		If point is specified, find_node() will search for a node with a
		pos attribute equal to point.

		outputs:
		other_node: node. The desired node dict.
		None: if point is not specified and there is no square one ahead of
		the robot, None is returned. This is useful for conditional statements
		in other methods and functions (see: move()).

		"""

		global all_nodes, current_dir, current_node, goal, targets
		global FORWARD, RIGHT, BACKWARD, LEFT

		pos = current_node['pos']


		if point != None:		# A specific point is given (exit, virus, etc.)
			node_found = False
			for other_node in all_nodes:
				if other_node['pos'] == point:
					node_found = True
					return other_node

			if not node_found:
				node_there = make_node(point)
				all_nodes.append(node_there)
				return node_there

		elif direction != None:	# A specific direction to check is given
			if direction == current_dir:
				if robot.sense_steps(robot.SENSOR_FORWARD) > 0:
					if current_dir == FORWARD:
						node_found = False
						for other_node in all_nodes:
							if other_node['pos'] == (pos[0], pos[1] + 1):
								node_found = True
								return other_node
						
						if not node_found:
							node_there = make_node((pos[0], pos[1] + 1))
							all_nodes.append(node_there)
							return node_there

					elif current_dir == RIGHT:
						node_found = False
						for other_node in all_nodes:
							if other_node['pos'] == (pos[0] + 1, pos[1]):
								node_found = True
								return other_node
						
						if not node_found:
							node_there = make_node((pos[0] + 1, pos[1]))
							all_nodes.append(node_there)
							return node_there

					elif current_dir == BACKWARD:
						node_found = False
						for other_node in all_nodes:
							if other_node['pos'] == (pos[0], pos[1] - 1):
								node_found = True
								return other_node
						
						if not node_found:
							node_there = make_node((pos[0], pos[1] - 1))
							all_nodes.append(node_there)
							return node_there

					elif current_dir == LEFT:
						node_found = False
						for other_node in all_nodes:
							if other_node['pos'] == (pos[0] - 1, pos[1]):
								node_found = True
								return other_node

						if not node_found:
							node_there = make_node((pos[0] - 1, pos[1]))
							all_nodes.append(node_there)
							return node_there
				
				else:
					return None

			elif direction == (current_dir + 1) % 4:
				if robot.sense_steps(robot.SENSOR_RIGHT) > 0:
					if current_dir == FORWARD:
						node_found = False
						for other_node in all_nodes:
							if other_node['pos'] == (pos[0] + 1, pos[1]):
								node_found = True
								return other_node
						
						if not node_found:
							node_there = make_node((pos[0] + 1, pos[1]))
							all_nodes.append(node_there)
							return node_there

					elif current_dir == RIGHT:
						node_found = False
						for other_node in all_nodes:
							if other_node['pos'] == (pos[0], pos[1] - 1):
								node_found = True
								return other_node
						
						if not node_found:
							node_there = make_node((pos[0], pos[1] - 1))
							all_nodes.append(node_there)
							return node_there

					elif current_dir == BACKWARD:
						node_found = False
						for other_node in all_nodes:
							if other_node['pos'] == (pos[0] - 1, pos[1]):
								node_found = True
								return other_node
						
						if not node_found:
							node_there = make_node((pos[0] - 1, pos[1]))
							all_nodes.append(node_there)
							return node_there

					elif current_dir == LEFT:
						node_found = False
						for other_node in all_nodes:
							if other_node['pos'] == (pos[0], pos[1] + 1):
								node_found = True
								return other_node

						if not node_found:
							node_there = make_node((pos[0], pos[1] + 1))
							all_nodes.append(node_there)
							return node_there
				else:
					return None

			elif direction == (current_dir - 1) % 4:
				if robot.sense_steps(robot.SENSOR_LEFT) > 0:
					if current_dir == FORWARD:
						node_found = False
						for other_node in all_nodes:
							if other_node['pos'] == (pos[0] - 1, pos[1]):
								node_found = True
								return other_node
						
						if not node_found:
							node_there = make_node((pos[0] - 1, pos[1]))
							all_nodes.append(node_there)
							return node_there

					elif current_dir == RIGHT:
						node_found = False
						for other_node in all_nodes:
							if other_node['pos'] == (pos[0], pos[1] + 1):
								node_found = True
								return other_node
						
						if not node_found:
							node_there = make_node((pos[0], pos[1] + 1))
							all_nodes.append(node_there)
							return node_there

					elif current_dir == BACKWARD:
						node_found = False
						for other_node in all_nodes:
							if other_node['pos'] == (pos[0] + 1, pos[1]):
								node_found = True
								return other_node
						
						if not node_found:
							node_there = make_node((pos[0] + 1, pos[1]))
							all_nodes.append(node_there)
							return node_there

					elif current_dir == LEFT:
						node_found = False
						for other_node in all_nodes:
							if other_node['pos'] == (pos[0], pos[1] - 1):
								node_found = True
								return other_node

						if not node_found:
							node_there = make_node((pos[0], pos[1] - 1))
							all_nodes.append(node_there)
							return node_there

				else:
					return None
		else:
			return None 	# Nothing specified


	def set_next_target():
		"""sorts known viruses based on taxicab distance from current node.

		inputs: None

		outputs:
		target: node dict. Refers to closest virus, or, if no viruses, a random
			exit

		"""
		global all_nodes, current_node, current_dir, goal, targets
		global FORWARD, RIGHT, BACKWARD, LEFT

		if robot.num_viruses_left() > 0:

			viruses = []

			for target in targets:
				if target['virus']:
					viruses.append(target)

			target = None
			min_dist = float('inf')
			for virus in viruses:
				if taxicab_dist(current_node['pos'], virus['pos']) < min_dist:
					min_dist = taxicab_dist(current_node['pos'], virus['pos'])
					target = virus

			if target is None:
				raise RuntimeError("""No exit has been activated due to
					unsolved vile viruses. Nothing further can be done in
					the maze.""")
			
			for node in all_nodes:
				node['dist'] = taxicab_dist(node['pos'], target['pos'])
				node['visited'] = False

			current_node['visited'] = True

			return target

		else:	# no viruses remain, so an exit is chosen
			target = targets[0]	# what exit is chosen is irrelevant

			for node in all_nodes:
				node['dist'] = taxicab_dist(node['pos'], target['pos'])
				node['visited'] = False
				
			current_node['visited'] = True

			return target


	def change_dir(direction):
		"""change robot orientation while maintaining current_dir.
		
		inputs:
		direction: int in range(0,4) - FORWARD, RIGHT, BACKWARD, or LEFT.
		
		outputs: None
		
		direction is first sanity-tested. If it passes and the robot's current
		direction is alreads accurate, nothing happens. Otherwise, the
		current_dir variable is changed to ensure that current_dir
		is still in range(0,4), and then the robot is reoriented with
		robot.turn_left() or robot.turn_right(), depending on whether 
		direction > current_dir or direction < current_dir.
		
		"""

		global all_nodes, current_dir, current_node, goal, targets
		global FORWARD, RIGHT, BACKWARD, LEFT
		
		if direction in range(0,4):
			if current_dir == direction:
				pass
			elif current_dir < direction:
				difference = direction - current_dir
				robot.turn_right(difference)
				current_dir += difference
			elif current_dir > direction:
				difference = current_dir - direction
				robot.turn_left(difference)
				current_dir -= difference
		else:
			raise ValueError("direction must be FORWARD (0), "
				"RIGHT (1), BACKWARD (2), or LEFT (3)")


	def find_dir(node):
		"""return the direction of movement required to reach a point.

		inputs:
		node: dict. containing the coordinates of a point

		outputs:
		direction: either FORWARD, BACKWARD, LEFT, or RIGHT

		This function is an intermediary which will reduce code reuse for
		determining how to proceed and how to move in the graph.

		"""

		global all_nodes, current_dir, current_node, goal, targets
		global FORWARD, RIGHT, BACKWARD, LEFT

		x = current_node['pos'][0]
		y = current_node['pos'][1]

		if node['pos'] == (x, y + 1):
			return FORWARD
		elif node['pos'] == (x + 1, y):
			return RIGHT
		elif node['pos'] == (x, y - 1):
			return BACKWARD
		elif node['pos'] == (x - 1, y):
			return LEFT


	def set_virus_points():
		"""compile all viruses in 10-sq radius, with objective coordinates.

		inputs: None

		outputs: None

		"""

		pos = current_node['pos']

		for virus in robot.sense_viruses():
			virus_node = find_node(point=(virus[0]+pos[0], virus[1]+pos[1]))
			if virus_node not in targets:
				targets.append(virus_node)


	def move(direction, num_steps=1, backtracking=False):
		"""wrapper function for robot.step_forward().
		
		inputs: 
		direction: int in range(0,4)
			Must equal either FORWARD, LEFT, BACKWARD, or RIGHT
		num_steps: int, as in robot.step_forward()
		backtracking: boolean.
		
		outputs: None
		
		Robot orients itself to face direction, then moves one step at a time,
		maintaining current position and a list of all nodes as it goes.

		"""
		global all_nodes, current_dir, current_node, goal, targets
		global FORWARD, RIGHT, BACKWARD, LEFT

		change_dir(direction)
		
		steps_taken = 0
		while steps_taken < num_steps:
			next_step = find_node(direction=current_dir)
			if next_step != None:
				robot.step_forward()

				if not backtracking:
					next_step['parent'] = current_node['pos']
				
				if not next_step['visited']:
					next_step['visited'] = True

				current_node = next_step

				for n in all_nodes:
					n['dist'] = taxicab_dist(current_node['pos'], n['pos'])

			steps_taken += 1


	def initialize():
		"""establishes the graph for the algorithm to traverse.

		inputs: None
		outputs: None

		"""

		global all_nodes, current_dir, current_node, goal, targets
		global FORWARD, RIGHT, BACKWARD, LEFT
		
		current_dir = 0
		
		current_node = make_node((0,0), visited=True)
		all_nodes = [current_node]

		targets = []

		for pos in robot.sense_viruses():
			targets.append(make_node(tuple(pos), virus=True))

		for pos in robot.sense_exits():
		 	targets.append(make_node(tuple(pos), exit=True))

		for node in targets:
			all_nodes.append(node)

		goal = set_next_target()


	def check_sides(): 
		"""checks sides to determine if backtracking is necessary.
		
		inputs: None

		outputs:
		next_dir: direction (int in range(0,4)) or None
		next_node: node dict or None

		"""

		global all_nodes, current_dir, current_node, goal, targets
		global FORWARD, RIGHT, BACKWARD, LEFT

		forward_node = find_node(direction=current_dir)
		right_node = find_node(direction=(current_dir + 1)%4)
		left_node = find_node(direction=(current_dir - 1)%4)


		# to iterate through the robot's neighbors, essentially.
		node_positions = [forward_node, right_node, left_node]
		# was having difficulty with normal if-statements
		# these list comprehensions became necessary
		node_positions = [x for x in node_positions if x != None]
		node_positions = [x for x in node_positions if not x['visited']]

		if len(node_positions) == 0:
			# There are no unvisited nodes adjacent to this node.
			return None, None
		
		else:
			node_positions = sorted(node_positions,
				key=lambda x: taxicab_dist(x['pos'], goal['pos']))

			return find_dir(node_positions[0]), node_positions[0]


	def check_targets():
		"""maintains target list as robot moves through the maze.

		inputs: None
		outputs: None

		"""

		global all_nodes, current_dir, current_node, goal, targets
		global FORWARD, RIGHT, BACKWARD, LEFT

		if current_node['virus'] and current_node in targets:
			targets.remove(current_node)
			current_node['virus'] = False
			
			set_virus_points()

			if current_node == goal:
				goal = set_next_target()


	def backtrack(next_node):
		"""moves towards start until unvisited nodes appear.

		inputs:
		next_node: node dict, or None

		outputs: None

		direction will most likely come from check_sides()[0]

		"""

		global all_nodes, current_dir, current_node, goal, targets
		global FORWARD, RIGHT, BACKWARD, LEFT
		
		options = next_node
		while not options:
			parent_node = find_node(point=current_node['parent'])
			move(find_dir(parent_node), backtracking=True)

			options = check_sides()[1]


	def search(next_dir, next_node):
		"""uses check_sides() to orient and direct robot.

		inputs:
		next_dir: direction (int in range(0,4)) or None
		next_node: node dict or None

		outputs: None
		
		"""

		global all_nodes, current_dir, current_node, goal, targets
		global FORWARD, RIGHT, BACKWARD, LEFT
	
		if next_node: # there are unvisited available nodes 
			move(next_dir)

		else:
			backtrack(next_node)


	initialize()

	direction, node = check_sides()
	# case handling: starting facing a dead end
	if not direction and not node:
		change_dir(BACKWARD)
		direction, node = check_sides()

	while True:
		search(direction, node)
		check_targets()
		direction, node = check_sides()
