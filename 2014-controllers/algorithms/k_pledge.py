# k_pledge.py
# Pledge Algorithm ("Kai-nda Pledge")
#
# This module is intended for use in simple contiguous and non-contiguous 
#     mazes. As opposed to the naive wallfollower, the Pledge Algorithm allows 
#     for an "escape" from an island-like maze portion which would otherwise 
#     trap a left or right-handed wallfollower. 
# This escape occurs when the turn counter returns to its initial state at zero. 
#
#
# Written by: Kai Atanasoff, with consultation of 
#     previous iterations written by Marcus Fedarko
# Edited by: Evan Spotte-Smith

def control_robot(robot):
    """utilizes Pledge Algorithm to follow walls, avoiding island-circling.

    inputs:
    robot: the robot object

    outputs: None
    
    """

    go_right = True
    num_turns = 0

    while True: # Finds the space forward, left and right, and moves accordingly
        space_fwd = robot.sense_steps(robot.SENSOR_FORWARD)
        space_right = robot.sense_steps(robot.SENSOR_RIGHT)
        space_left = robot.sense_steps(robot.SENSOR_LEFT)

        if num_turns > 3:    # Switches turn direction preference
            go_right = False

        if num_turns < 1:    # Maintains turn direction preference
            go_right = True

        if go_right:    # Wallfollows according to the initial preference
            if space_right > 0:
                robot.turn_right(1)
                num_turns += 1    # Turn counter increment
                robot.step_forward(1)
            elif space_fwd > 0:
                robot.step_forward(1)
            elif space_left > 0:
                robot.turn_left(1)
                num_turns -= 1    # Turn counter decrement
                robot.step_forward(1)
            else:    # Escape dead-end
                robot.turn_right(2)

        else:    # Wallfollows according to secondary preference
            if space_left > 0:
                robot.turn_left(1)
                num_turns -= 1
                robot.step_forward(1)
            elif space_fwd > 0:
                robot.step_forward(1)
            elif space_right > 0:
                robot.turn_right(1)
                num_turns += 1
                robot.step_forward(1)
            else:
                robot.turn_right(2)