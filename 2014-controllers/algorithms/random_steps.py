# random_steps.py
# Random Mouse Algorithm (Step-Focused)
#   also known as randomTPP.py
#
# In this algorithm, the robot acts like a mouse with no memory or reaction to
#   stimuli while attempting to solve a maze. The robo-mouse evaluates possible
#   directions before every step and chooses a random direction via the
#   random.choice() function from Python's Standard Library.
#
# Theoretically, this will solve any maze perfectly, given sufficient time.
#   In reality, though far from perfect, Random Mouse is able
#   to reach most, if not all, viruses in a maze in the allotted time.
#
# PRAISE BE TO THE HELIX! DOWN WITH THE DOMISTS! ANARCHY! ANARCHY!
#
# Written by: Kai Atanasoff and Marcus Fedarko, with inspiration from the
#   popular crowdsourcing game Twitch Plays Pokemon
# 
# Edited by: Evan Spotte-Smith

from random import choice # the harbinger of insanity

def control_robot(robot):
    """implements Random Mouse (Random TPP) "pathfinding algorithm". #yoloswag

    inputs:
    robot: the robot object

    outputs: None (more accurately, hilarity)

    Picks a random direction and moves there.

    Swag me out, Random TPP.

    """
  
    # simple directional identifier constants
    AHEAD = "ahead"
    LEFT = "left"
    RIGHT = "right"
    
    while True:
        step_ahead = robot.sense_steps(robot.SENSOR_FORWARD)
        step_left = robot.sense_steps(robot.SENSOR_LEFT)
        step_right = robot.sense_steps(robot.SENSOR_RIGHT)
        
        # based on sensing results, calculate a list of viable directions
        # that the robot could go in
        
        opt_lengths = [step_ahead, step_left, step_right]
        potential_options = [AHEAD, LEFT, RIGHT]
        viable_options = []
        
        c = 0
        for opt in opt_lengths:
            if opt > 0:
                viable_options.append(potential_options[c])
            c += 1
            
        if len(viable_options) == 0:
            robot.turn_right(2)
            robot.step_forward()
        else:
            # All hail the Helix Fossil
            # He tests our will, but never leads us astray
            the_one_choice = choice(viable_options)
            if the_one_choice == AHEAD:
                robot.step_forward(1)
            elif the_one_choice == LEFT:
                robot.turn_left(1)
                robot.step_forward(1)
            elif the_one_choice == RIGHT:
                robot.turn_right(1)
                robot.step_forward(1)