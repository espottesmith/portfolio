# rave_party.py
# 
# A module designed to move the robot according to no particular algorithm
#   at all. This was, is, and will remain in eternity a terrible idea.
#   But hey, it looks pretty cool, right!
#
# Since the robot loses its focus as soon as it locates all the viruses, this
#   is a very impractical choice - the level exit can never be found. It's
#   essentially a wall-follower, just less effective and more hilarious.
#
#
# Written with a sense of shame by: Marcus Fedarko
# Edited by: Kai Atanasoff and Evan Spotte-Smith
#
# IMPORTANT NOTE: This probably isn't safe for people with epilepsy or
#   motion sickness.

# the harbinger of insanity part 3...Party Hard with a Vengeance
from random import randint


def control_robot(robot):   
    """wallfollows until the robot feels like it (no viruses are left)
    
    inputs: robot
    
    outputs: None (hilarity)
    
    """

    n = robot.num_viruses_left()    # Popped a [CENSORED], I'm sweating (WOO!).
    while n > 1:
        if robot.sense_steps(robot.SENSOR_LEFT) >= 1:
            robot.turn_left()
            robot.step_forward()
        elif robot.sense_steps(robot.SENSOR_FORWARD) >= 1:
            robot.step_forward()
        elif robot.sense_steps(robot.SENSOR_RIGHT) >= 1:
            robot.turn_right()
            robot.step_forward()
        else:
            robot.turn_right(2)
        n = robot.num_viruses_left()
            
    # prepare thyself
    while True:
        robot.turn_left(randint(0, 3))
        robot.turn_right(randint(0, 3))