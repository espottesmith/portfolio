# wall_follower.py
# Wall-Following Algorithm
#
# A basic wall-following algorithm. This moves the robot along a wall,
#   and should work adequately for all contiguous mazes.
# Note that the integer FOLLOW is configurable. Set it to 0 to get the
#   robot to do left-hand wall following, and set it to 1 to get the robot
#   to do right-hand wall following.
# 
# Notes/Questions:
#   This is really ineffective and inefficient for most mazes. It's
#   included here as a reference and last resort.
# 
# 
# Written by: Marcus Fedarko

FOLLOW = 0

def control_robot(robot):

    if FOLLOW == 0:
        while True:
            if robot.sense_steps(robot.SENSOR_LEFT) >= 1:
                robot.turn_left()
                robot.step_forward()
            elif robot.sense_steps(robot.SENSOR_FORWARD) >= 1:
                robot.step_forward()
            elif robot.sense_steps(robot.SENSOR_RIGHT) >= 1:
                robot.turn_right()
                robot.step_forward()
            else:
                robot.turn_right(2)

    elif FOLLOW == 1:
        while True:
            if robot.sense_steps(robot.SENSOR_RIGHT) >= 1:
                robot.turn_right()
                robot.step_forward()
            elif robot.sense_steps(robot.SENSOR_FORWARD) >= 1:
                robot.step_forward()
            elif robot.sense_steps(robot.SENSOR_LEFT) >= 1:
                robot.turn_left()
                robot.step_forward()
            else:
                robot.turn_left(2)
