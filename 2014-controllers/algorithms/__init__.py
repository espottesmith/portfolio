# __init__.py
# 
# This file only serves to establish programmatically as a Python package. 
#   If we'd want to use initialization code for whenever an algorithm was 
#   imported, we could add it here.