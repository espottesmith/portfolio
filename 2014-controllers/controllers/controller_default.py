# controller_default.py
# Controller Placeholder
#
# This file is here so that it can be submitted with the rest of our code.
#   Its contents will be applied to all of the controller_N[x] files for the
#   challenge day, saving us time.
#
# Written by: Marcus Fedarko
#
# Notes:
#   The string constant ALGORITHM can be changed to the name of whichever
#     algorithm module is desired to use. If necessary, the contents of any
#     controller file can be replaced with the full code for an algorithm module
#     in order to customize certain aspects of the algorithm for specific mazes.
#   In order for this to work, the algorithm files must be contained in the same
#     folder as the controller files; otherwise, the executed import statement
#     will fail.
#
#   ALGORITHM options:
#     best_first
#     d_star_lite
#     depth_first
#     k_pledge
#     random_passages
#     random_steps
#     rave_party
#     wall_follower

ALGORITHM = "best_first"

def control_robot(robot):
    """moves the robot through a maze using the indicated algorithm.

    inputs:
    robot: the robot object
    
    outputs: None

    """
    
    exec "import %s" % (ALGORITHM)
    eval("%s.control_robot(robot)" % (ALGORITHM))
