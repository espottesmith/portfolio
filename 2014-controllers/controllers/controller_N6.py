# controller_N6.py
# Controller Placeholder
#
# Replace this with the full text of one of our algorithm files (located
#    in /algorithms/) on challenge day.
# Right now, this is just a placeholder - it does nothing.

def control_robot(robot):
    """ Control robot.

    Keyword arguments:
    robot -- Robot object that must be controlled through the maze.

    """
    pass