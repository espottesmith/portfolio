#Written By Tomi Okiji



def control_robot(robot):

	global all_nodes, current_dir, current_node, goal, targets
	global FORWARD, RIGHT, BACKWARD, LEFT

	current_dir = 0
        
	FORWARD = 0
	RIGHT = 1
	BACKWARD = 2
	LEFT = 3


	def taxicab_dist(beginning, end):
		"""standard implementation of two-dimensional taxicab distance formula

	    inputs:
	    beginning: 2-tuple. The start coordinates.
	    end: 2-tuple. The end coordinates.

	    outputs:
	    output of two-dimensional distance formula in taxicab geometry:
	        d = (x_2 - x_1) + (y_2 - y_1)
	    with x_1 and y_1 originating from this node's location.

	    In taxicab geometry, a discrete, non-continuous geometric system,
	    points are defined unconventionally: rather than having points at all
	    real numbers, there are only points at every integral value, ie -1, 0,
	    1, etc. As such, a straight like can no longer be defined as the
	    shortest distance between two points, as often, one cannot make a
	    straight line between two points (like between (0,0) and (1,1), in
	    which the optimal path either goes through (0,1) or through (1,0)).
	    This function was chosen, as opposed to the traditional distance
	    formula math.sqrt((x_2 - x_1)**2 + (y_2 - y_1)**2), because the robot
	    cannot move diagonally, but can only go FORWARD, RIGHT, BACKWARD, or 
	    LEFT.

	    ***NOTE: THIS IS THE SAME THING AS A MANHATTAN HEURISTIC FUNCTION.***

	    Does not necessarily produce the actual distance or cost from a start
	    point to the goal.

	    """

		return abs(end[0] - beginning[0]) + abs(end[1] - beginning[1])


	def make_node(pos, parent=None, visited=False, virus=False, exit=False):
		"""creates a dict shell for a objective-coordinate node.

		inputs:
		pos: 2-tuple. The position that this node holds in the graph

		outputs:
		node: A dict with position, parent, and visited values
		"""
        
		global all_nodes, current_dir, current_node, goal, targets
		global FORWARD, RIGHT, BACKWARD, LEFT

		return {
			'pos':pos,
			'parent':parent,
			'options':[]
			'visited':visited,
			'virus':virus,
			'exit':exit,
			'dist':0
		}


	def find_node(point=None):
		"""determine if a particular node exists, returns node if possible.

		inputs:
		point: a 2-tuple defining coordinates. By default, this attribute is
		set to None, meaning that find_node() will use whatever square is
		one ahead of the robot as the Node to search for.
		If point is specified, find_node() will search for a node with a
		pos attribute equal to point.

		outputs:
		other_node: node. The desired node dict.
		None: if point is not specified and there is no square one ahead of
		the robot, None is returned. This is useful for conditional statements
		in other methods and functions (see: move()).

		"""
		global all_nodes, current_dir, current_node, goal, targets
		global FORWARD, RIGHT, BACKWARD, LEFT

		pos = current_node['pos']


		if point != None:		# A specific point is given (exit, virus, etc.)
			node_found = False
			for other_node in all_nodes:
				if other_node['pos'] == point:
					node_found = True
					return other_node

			if not node_found:
				node_there = make_node(point)
				all_nodes.append(node_there)
				return node_there


		elif robot.sense_steps(robot.SENSOR_FORWARD) > 0:
			if current_dir == FORWARD:
				node_found = False
				for other_node in all_nodes:
					if other_node['pos'] == (pos[0], pos[1] + 1):
						node_found = True
						return other_node
				
				if not node_found:
					node_there = make_node((pos[0], pos[1] + 1))
					all_nodes.append(node_there)
					return node_there

			elif current_dir == RIGHT:
				node_found = False
				for other_node in all_nodes:
					if other_node['pos'] == (pos[0] + 1, pos[1]):
						node_found = True
						return other_node
				
				if not node_found:
					node_there = make_node((pos[0] + 1, pos[1]))
					all_nodes.append(node_there)
					return node_there

			elif current_dir == BACKWARD:
				node_found = False
				for other_node in all_nodes:
					if other_node['pos'] == (pos[0], pos[1] - 1):
						node_found = True
						return other_node
				
				if not node_found:
					node_there = make_node((pos[0], pos[1] - 1))
					all_nodes.append(node_there)
					return node_there

			elif current_dir == LEFT:
				node_found = False
				for other_node in all_nodes:
					if other_node['pos'] == (pos[0] - 1, pos[1]):
						node_found = True
						return other_node

				if not node_found:
					node_there = make_node((pos[0] - 1, pos[1]))
					all_nodes.append(node_there)
					return node_there

			else:
				return None 	# No node in specified area


	def change_dir(direction):
		"""change robot orientation while maintaining current_dir.
		
		inputs:
		direction: int in range(0,4) - must equal either FORWARD, RIGHT, BACKWARD, or LEFT.
		
		outputs: None
		
		direction is first sanity-tested. If it passes and the robot's current
		direction is alreads accurate, nothing happens. Otherwise, the
		current_dir variable is changed to ensure that current_dir
		is still in range(0,4), and then the robot is reoriented with
		robot.turn_left or robot.turn_right, depending on whether 
		direction > current_dir or direction < current_dir.

		Not fully optimal (a left turn while facing FORWARD requires 3 calls of
		the robot.turn_right() method, for instance), but this implementation
		simplifies code and increases readability.
		
		"""

		global all_nodes, current_dir, current_node, goal, targets
		global FORWARD, RIGHT, BACKWARD, LEFT
		
		if direction in range(0,4):
			if current_dir == direction:
				pass
			elif current_dir < direction:
				difference = direction - current_dir
				robot.turn_right(difference)
				current_dir += difference
			elif current_dir > direction:
				difference = current_dir - direction
				robot.turn_left(difference)
				current_dir -= difference
		else:
			raise ValueError("""direction must be FORWARD (0),
				RIGHT (1), BACKWARD (2), or LEFT (3)""")


	def find_dir(node):
		"""return the direction of movement required to reach a point

		inputs:
		node: dict. containing the coordinates of a point

		outputs:
		direction: either FORWARD, BACKWARD, LEFT, or RIGHT

		This function is an intermediary which will reduce code reuse for
		determining how to proceed and how to move in the graph.

		"""

		global all_nodes, current_dir, current_node, goal, targets
		global FORWARD, RIGHT, BACKWARD, LEFT

		x = current_node['pos'][0]
		y = current_node['pos'][1]

		if node['pos'] == (x, y + 1):
			return FORWARD
		elif node['pos'] == (x + 1, y):
			return RIGHT
		elif node['pos'] == (x, y - 1):
			return BACKWARD
		elif node['pos'] == (x - 1, y):
			return LEFT


 def make_list():
	#NOTE!!!: function not finished  

 	#gets robots currents position
 	#Since the G cost for each node equals 0 
 	# F = H 
 	#The robot will calculate the H cost of each of the surrounding nodes 
	#after add the node with the lowest H cost to a closed list
	#once path leading to exit is made 
	#travail to exit 
	
	
	#outputs:
	#closed_list with path going to exit 
	

	
	forward_node = find_node(direction=current_dir)
	right_node = find_node(direction=(current_dir + 1)%4)
	left_node = find_node(direction=(current_dir - 1)%4)
	back_node = find_node(direction= (current_dir +2)%4)

	nodes = [forward_node,right_node,left_node,back_node]

	exit_dic = robot.sense_exits()

	#the distance that is evaluate determines if robot moves or not 
	#set diestance to foward node so that other node may be compared 
	distance = taxicab_dist(foward_node,exit_dic)
	
	#the node that will be moved to based on taxi cab distance 
	#arbitrarily chose to make foward_node first 
	next_move = foward_node 

	for node in nodes: 

		tc_dist = taxicab_dist(node,exit_dic)
		if(tc_dist < distance):
			next_move = node 

	close_list.append(next_move)

		
	return closed_list #return closed list that is used to move 
		


	def move(closed_list):    

	#this function will use the list returned by the make_list 
	#function to get to the exit 

	#inputs: 
	#list- list which is used to get to exit 

	for node in closed_list: 
		direction = find_dir(node)
		change_direction(direction)

		robot.step_foward()
