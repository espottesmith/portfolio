# Robot.py
#	Present robot system defines points (of exits, viruses) relatively:
#		Ex: If robot.sense_viruses() returned (1,1), then the virus would
#			be one square ahead of the robot and one square to its right.
#	Ideally, it would be possible to model the maze as a graph.
#		This requires objective coordinate scheme.
#	Though the robot does not know all points in the maze, it can use
#		coordinate tuples (x,y) to model where it is and where it has been.
#	Through sorting, one can then construct paths logically.
#	This protocol will be helpful for any non-wall-following algorithms
#		being devised (A*, for instance).
#	Memory and runtime for graph-point operations should not be an issue,
#		as early graphs have relatively few squares to consider.
#
#
# Notes/Questions:
#   This is really great, Evan. Thanks for all of your work!
# 
# Written by: Evan Spotte-Smith, with suggestions from Kai Atanasoff


from Node import Node


class Robot(object):
	"""a wrapper class to allow for desired robot and Node manipulations.

	This class initially took the form of a series of methods and functions.
	State was assigned to the robot itself, with the assumption that robot was
	a class that could be altered with new attributes (see __init__()).

	During testing, it appeared as if this assumption was false. However, the
	behavior defined in these methods was still desirable, so this class was
	built atop them.

	"""

	def __init__(self, robot):
		"""initialize a graph for a pathfinding algorithm.

		inputs:
		self
		robot: the robot object

		outputs: None

		Defines a set of attributes created by LRHS to extend the robot object.

		"""

		# not sure if this is necessary, but it sure would make things easier.
		self.robot = robot

		self.FORWARD = 0
		self.RIGHT = 1
		self.BACKWARD = 2
		self.LEFT = 3


		self.all_nodes = []

		self.current_dir = FORWARD
		# arbitrary choice (defines perspective of the graph)

		self.current_node = Node((0,0), start=True)
		self.all_nodes.append(self.current_node)

		self.all_visited = [self.current_node, ]
		self.all_exits = []

		# find all exits
		# because original pos is (0,0), objective coords = relative coords.
		for exit in robot.sense_exits():
			self.all_exits.append(self.find_node_there(self.robot, point=exit))

		self.set_neighborhood(robot, self.current_node)

		self.viruses = self.set_virus_points(self.robot)

		self.targets = self.robot.num_viruses_left()


	def aim(self, goal):
		"""directs the robot to a new goal Node, using all Nodes in the maze"""

		for node in self.all_nodes:
			node.change_goal(goal)


	def sort_attack_path(self):
		"""sorts known viruses based on taxicab distance from current node.

		inputs:
		self

		outputs:
		the list known_viruses, sorted based on the distance of each point to
		the current location, based on taxicab geometry.

		***NOTE: FUNCTION MAY BE ELIMINATED OR JOINED WITH A LARGER FUNCTION.***

		"""

		return sorted(self.known_viruses, key=Node.taxicab_dist)


	def change_dir(self, robot, direction):
		"""change robot orientation while maintaining robot.current_dir.
		
		inputs:
		self
		robot: the robot object
		direction: int in range(0,4)
			Must equal either FORWARD, LEFT, BACKWARD, or RIGHT
		
		outputs: None
		
		direction is first sanity-tested. If it passes and the robot's current
		direction is alreads accurate, nothing happens. Otherwise, the
		self.current_dir variable is changed to ensure that self.current_dir
		is still in range(0,4), and then the robot is reoriented with
		robot.turn_left or robot.turn_right, depending on whether 
		direction > self.current_dir or direction < self.current_dir.

		Not fully optimal (a left turn while facing FORWARD requires 3 calls of
		the robot.turn_right() method, for instance), but this implementation
		simplifies code and increases readability.
		
		"""
		
		if direction in range(0,4):
			if self.current_dir == direction:
				pass
			elif self.current_dir < direction:
				difference = direction - self.current_dir
				robot.turn_right(difference)
				self.current_dir += difference
			elif self.current_dir > direction:
				difference = self.current_dir - direction
				robot.turn_left(difference)
				self.current_dir -= difference
		else:
			raise ValueError("""direction must be FORWARD (0),
				RIGHT (1), BACKWARD (2), or LEFT (3)""")


	def find_dir(self, node):
		"""return the direction of movement required to reach a particular point

		inputs:
		self
		node: Node. containing the coordinates of a point

		outputs:
		direction: either FORWARD, BACKWARD, LEFT, or RIGHT

		This function is an intermediary which will reduce code reuse for
		determining how to proceed and how to move in the graph.

		"""

		x = self.current_node.pos[0]
		y = self.current_node.pos[1]

		if node.pos == (x, y + 1):
			return self.FORWARD
		elif node.pos == (x, y - 1):
			return self.BACKWARD
		elif node.pos == (x + 1, y):
			return self.RIGHT
		else:
			return self.LEFT


	def find_node_there(self, robot, point=None):
		"""determine if a particular node exists, returns node if possible.

		inputs:
		robot: the robot object
		point: a 2-tuple defining coordinates. By default, this attribute is
		set to None, meaning that find_node_there() will use whatever square is
		one ahead of the robot as the Node to search for.
		If point is specified, find_node_there() will search for a node with a
		pos attribute equal to point.

		outputs:
		other_node: Node. The desired Node object.
		None: if point is not specified and there is no square one ahead of
		the robot, None is returned. This is useful for conditional statements
		in other methods and functions (see: set_virus_points(),
		set_exit_points(), set_neighborhood(), or move()).

		***NOTE: Code reuse is a serious problem in this function. Can it be
		limited? Is there an easier way to do this?***

		"""

		if point != None:		# A specific point is given (exit, virus, etc.)
			node_found = False
			for other_node in self.all_nodes:
				if other_node.pos == point:
					node_found = True
					return other_node

			if not node_found:
				node_there = Node(point)
				self.all_nodes.append(node_there)
				return node_there


		elif robot.sense_steps(robot.SENSOR_FORWARD):
			if self.current_dir == FORWARD:
				node_found = False
				for other_node in self.all_nodes:
					if other_node.pos == (self.current_node.pos[0], \
						self.current_node.pos[1] + 1):
						node_found = True
						return other_node
				
				if not node_found:
					node_there = Node((self.current_node.pos[0], \
						self.current_node.pos[1] + 1))
					self.all_nodes.append(node_there)
					return node_there

			elif self.current_dir == RIGHT:
				node_found = False
				for other_node in self.all_nodes:
					if other_node.pos == (self.current_node.pos[0] + 1, \
						self.current_node.pos[1]):
						node_found = True
						return other_node
				
				if not node_found:
					node_there = Node((self.current_node.pos[0] + 1, \
						self.current_node.pos[1]))
					self.all_nodes.append(node_there)
					return node_there

			elif self.current_dir == BACKWARD:
				node_found = False
				for other_node in self.all_nodes:
					if other_node.pos == (self.current_node.pos[0], \
						self.current_node.pos[1] - 1):
						node_found = True
						return other_node
				
				if not node_found:
					node_there = Node((self.current_node.pos[0], \
						self.current_node.pos[1] - 1))
					self.all_nodes.append(node_there)
					return node_there

			elif self.current_dir == LEFT:
				node_found = False
				for other_node in self.all_nodes:
					if other_node.pos == (self.current_node.pos[0] - 1, \
						self.current_node.pos[1]):
						node_found = True
						return other_node

				if not node_found:
					node_there = Node((self.current_node.pos[0] - 1, \
						self.current_node.pos[1]))
					self.all_nodes.append(node_there)
					return node_there

			else:
				return None 	# No node in specified area 


	def set_virus_points(self, robot):
		"""compile all viruses in 10-sq radius, with objective coordinates.

		inputs:
		robot: the robot object

		outputs: Depends
		If robot.known_viruses has already been instantiated, this method
		merely appends the newly-found viruses to robot.known_viruses.
		Otherwise, a list of viruses is actually returned, to be used to
		define the robot.known_viruses attribute.

		"""

		if self.known_viruses:		# if variable has been instantiated already
			for virus_loc in robot.sense_viruses():
				virus_node = find_node_there(robot, point=( \
					virus_loc[0] + self.current_node.pos[0], \
					virus_loc[1] + self.current_node.pos[1]) \
					)
				if virus_node not in self.known_viruses:
					self.known_viruses.append(virus_node)
		
		else:
			known_viruses = []
			for point in self.sense_viruses():
				virus_node = find_node_there(robot, point=( \
					virus_loc[0] + self.current_node.pos[0], \
					virus_loc[1] + self.current_node.pos[1]) \
					)
				known_viruses.append(virus_node)

			return known_viruses


	def set_neighborhood(self, robot, node):
		"""hi there, neighbor. This defines a Node's neighborhood.

		inputs:
		self: you're perfect just the way you are
		robot: a friendly robot object
		node: a very special and unique Node

		outputs:
		a beautiful list of neighbors for the input node

		***NOTE: THIS FUNCTION ALSO ADDS THE INPUT NODE TO THE OTHER NODE'S
		neighborhood, IF NOT ALREADY PRESENT THERE***

		***EASTER_EGG_NOTE: THE AUTHORS OF THIS CODE REFERENCE 
		"Mr. Roger's Neighborhood", A CHILDHOOD FAVORITE. 
								RIP 
							FRED ROGERS
							B: 3/20/1928
							D: 2/27/2003***

		"""

		proper_dir = self.current_dir
		num_turns = 0

		while num_turns < 4:
			node_ahead = self.find_node_there(robot)
			if node_ahead != None and node_ahead not in node.neighborhood:
				# it's a beautiful day
				node.neighborhood.append(node_ahead)

			robot.turn_right()
			num_turns += 1

		for neighbor in neighborhood:
			if node not in neighbor.neighborhood:
				neighbor.neighborhood.append(node)


	def move(self, robot, direction, num_steps=1):
		"""wrapper function for robot.step_forward().
		
		inputs: 
		robot: the robot object
		direction: int in range(0,4)
			Must equal either FORWARD, LEFT, BACKWARD, or RIGHT
		num_steps: int, as in robot.step_forward()
		
		outputs: None
		
		Robot orients itself to face direction, then moves one step at a time,
		maintaining current position and a list of all nodes as it goes.
		The main purpose for this is to ensure that virus sweeping can be
		constant. With each step, the set_virus_points() function, a wrapper
		for robot.sense_viruses(), is called, increasing the likelihood that
		any and all viruses that can be found will be found as quickly as 
		possible.
		
		Unfortunately, the insistence on rigorous sweeping and location
		maintenance decreases the speed of this method. Further testing must be
		undergone to determine the value-to-cost ratio of this particular
		trade-off.
		
		"""

		self.change_dir(direction)
		
		steps_taken = 0
		while steps_taken < num_steps:
			next_step = self.find_node_there(robot)
			if next_step != None:
				next_step.change_parent(self.current_node)
				self.current_node = next_step
				if self.current_node not in self.all_visited:
					self.all_visited.append(robot.current_node)

				robot.step_forward()
			
			# constant sweeping ensures that no viruses are missed.
			self.set_virus_points()
			steps_taken += 1
