# depth_first_passages.py
# Depth-First Search Algorithm (Passage-Focused)
#
# In this algorithm, the robot naively checks all the passages it encounters in
# accordance with the theoretical definition of Depth-First-Search (DFS).
#
# For illustrative purposes, the robot's path might resemble the following
# ASCII schematic:
#
#     R
#    / \
#   3  /\__1
#     2 
#
# The robot goes down the first set of passages it finds/chooses (1), then
# backtracks to and heads down the closest junction (2), and continues doing
# this until it decides it has exhausted its possibilities.
#
# Like its sibling algorithm random_passages, this algorithm is not guaranteed
# to solve a maze perfectly. It will skip "holes" in the sides of some passages
# and is therefore not exhaustive. However, it should at least make a sizeable
# dent in most mazes - it's a good catch-all for the majority of mazes.
#
#
# Written by: Marcus Fedarko, with consultation of 
#     previous iterations written by Tomi Okiji

def control_robot(robot):
    """implements a passage-based depth first search algorithm.

    inputs:
    robot: the robot object

    outputs: None

    """
    
    # simple directional identifier constants
    AHEAD = "ahead"
    LEFT = "left"
    RIGHT = "right"
    
    # contains a list of coordinates relative to the robot's starting position
    xpos = 0
    ypos = 0
    # for convenience: 0 = FORWARD, 1 = RIGHT, 2 = BACKWARD, 3 = LEFT -- all
    # relative to starting orientation
    orientation = 0
    # visited_junctions is of the form (x, y):"[A][L][R]"
    # tracks 3 possible passages from each junction (discounting backwards)
    visited_junctions = {(0, 0):""}
    
    turning_around = False
    
    while True:
        if ((xpos, ypos) not in visited_junctions.keys()):
            visited_junctions[(xpos, ypos)] = ""
            
        visited_passages = visited_junctions[(xpos, ypos)]
        
        step_ahead = 0
        step_left = 0
        step_right = 0
        
        # adjust step_ values based on orientation. Note that the sensors
        # used follow a cyclical pattern through each block.
        if 'A' not in visited_passages:
            if orientation == 0:
                step_ahead = robot.sense_steps(robot.SENSOR_FORWARD)
            elif orientation == 1:
                step_ahead = robot.sense_steps(robot.SENSOR_LEFT)
            elif orientation == 2:
                robot.turn_left()
                step_ahead = robot.sense_steps(robot.SENSOR_RIGHT)
                robot.turn_right()
            elif orientation == 3:
                step_ahead = robot.sense_steps(robot.SENSOR_RIGHT)
                
        if 'L' not in visited_passages:
            if orientation == 0:
                step_left = robot.sense_steps(robot.SENSOR_LEFT)
            elif orientation == 1:
                robot.turn_left()
                step_ahead = robot.sense_steps(robot.SENSOR_RIGHT)
                robot.turn_right()
            elif orientation == 2:
                step_ahead = robot.sense_steps(robot.SENSOR_RIGHT)
            elif orientation == 3:
                step_ahead = robot.sense_steps(robot.SENSOR_FORWARD)
                
        if 'R' not in visited_passages:
            if orientation == 0:
                step_right = robot.sense_steps(robot.SENSOR_RIGHT)
            elif orientation == 1:
                step_left = robot.sense_steps(robot.SENSOR_FORWARD)
            elif orientation == 2:
                step_left = robot.sense_steps(robot.SENSOR_LEFT)
            elif orientation == 3:
                robot.turn_left()
                step_ahead = robot.sense_steps(robot.SENSOR_RIGHT)
                robot.turn_right()
        
        # based on sensing results, calculate a list of viable directions
        # that the robot could go in
        
        opt_lengths = [step_ahead, step_left, step_right]
        potential_options = [AHEAD, LEFT, RIGHT]
        viable_options = []
        
        c = 0
        for opt in opt_lengths:
            if opt > 0:
                viable_options.append(potential_options[c])
            c += 1

        if len(viable_options) == 0:
            # If we get here twice in a row, raise an error - there's nowhere
            # to go
            if turning_around:
                raise RuntimeError, "No possible paths from current junction."
            turning_around = True
            robot.turn_right(2)
            continue
            
        else:
            turning_around = False
            
            # make a choice about which junction to pursue. for convenience,
            # we just pick the first option (giving precedence to A -> L -> R)
            the_one_choice = viable_options[0]
            if the_one_choice == AHEAD:
                robot.step_forward(step_ahead)
                visited_junctions[(xpos, ypos)] = visited_junctions[(xpos, ypos)] + "A"
                if (xpos, ypos + step_ahead) not in visited_junctions.keys():
                    visited_junctions[(xpos, ypos + step_ahead)] = ""
            elif the_one_choice == LEFT:
                robot.turn_left(1)
                robot.step_forward(step_left)
                visited_junctions[(xpos, ypos)] = visited_junctions[(xpos, ypos)] + "L"
                if (xpos + step_left, ypos) not in visited_junctions.keys():
                    visited_junctions[(xpos + step_left, ypos)] = ""
            elif the_one_choice == RIGHT:
                robot.turn_right(1)
                robot.step_forward(step_right)
                visited_junctions[(xpos, ypos)] = visited_junctions[(xpos, ypos)] + "R"
                if (xpos - step_right, ypos) not in visited_junctions.keys():
                    visited_junctions[(xpos - step_right, ypos)] = ""