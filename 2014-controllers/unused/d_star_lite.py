# d_star_lite.py
# 
# This module implements the D* Lite algorithm with backtracking.
# The inventors of D* Lite, Koenig and Likhachev, essentially ignored the
# 	possibility of impossible paths in defining their algorithm.
# 	They noted that if the g-cost of the current node was at any point 
# 	infinite, there was no way to get to the goal, but they never prescribed a
# 	remedy for that situation. To this end, we add backtracking to D* Lite.
# The robot will follow the path indicated by shortest_path() until the the 
# 	'ahead' value of the current node becomes float('inf'), then will go back
# 	along its path until it finds new nodes for which the same is not true.
#  
# 
# Written by: Evan Spotte-Smith
# Edited by: Kai Atanasoff
# Tested by: Evan Spotte-Smith and Kai Atanasoff


def control_robot(robot):
	"""implements D* Lite pathfinding algorithm.

	inputs: 
	robot: the robot object

	outputs: None

	Uses D* Lite to systematically destroy viruses. Searches until a virus is
	located, then eliminates viruses one by one. When there are no more
	viruses, goes to any exit (if multiple).

	"""

	global all_nodes, current_node, current_dir, goal, queue, targets
	global FORWARD, RIGHT, BACKWARD, LEFT

	# define directional variables
	FORWARD = 0
	RIGHT = 1
	BACKWARD = 2
	LEFT = 3


	def taxicab_dist(beginning, end):
		"""standard implementation of two-dimensional taxicab distance formula

	    inputs:
	    beginning: 2-tuple. The start coordinates.
	    end: 2-tuple. The end coordinates.

	    outputs:
	    output of two-dimensional distance formula in taxicab geometry:
	        d = (x_2 - x_1) + (y_2 - y_1)
	    with x_1 and y_1 originating from this node's location.

	    In taxicab geometry, a discrete, non-continuous geometric system,
	    points are defined unconventionally: rather than having points at all
	    real numbers, there are only points at every integral value, ie -1, 0,
	    1, etc. As such, a straight like can no longer be defined as the
	    shortest distance between two points, as often, one cannot make a
	    straight line between two points (like between (0,0) and (1,1), in
	    which the optimal path either goes through (0,1) or through (1,0)).
	    This function was chosen, as opposed to the traditional distance
	    formula math.sqrt((x_2 - x_1)**2 + (y_2 - y_1)**2), because the robot
	    cannot move diagonally, but can only go FORWARD, RIGHT, BACKWARD, or 
	    LEFT.

	    ***NOTE: THIS IS THE SAME THING AS A MANHATTAN HEURISTIC FUNCTION.***

	    Does not necessarily produce the actual distance or cost from a start
	    point to the goal.

	    """

		return abs(end[0] - beginning[0]) + abs(end[1] - beginning[1])


	def make_node(pos, parent=None, virus=False, exit=False):
		"""creates a dict shell for a objective-coordinate node.

		inputs:
		pos: 2-tuple. The position that this node holds in the graph
		parent: 2-tuple. The position of the parent node (for backtracking)
		virus: Boolean. Determines if this point has a virus in it.
		exit: Boolean. As above, but for exits.

		outputs:
		node: A dict with position, parent, neighborhood, dist, and status
		(virus, exit) attributes

		"""
		global all_nodes, current_node, current_dir, goal, queue, targets
		global FORWARD, RIGHT, BACKWARD, LEFT


		back = taxicab_dist(pos, current_node['pos'])
		ahead = taxicab_dist(pos, goal['pos'])
		total = back + ahead

		x = pos[0]
		y = pos[1]
		neighborhood = [(x, y+1), (x+1, y), (x, y-1), (x-1, y)]

		predecessors = []
		successors = []
		for other_pos in neighborhood:
			if taxicab_dist(other_pos, goal['pos']) < ahead:
				successors.append(other_pos)
			else:
				predecessors.append(other_pos)

		return {
			'pos':pos,
			'parent':parent,
			'neighborhood':neighborhood,
			'predecessors':predecessors,
			'successors':successors,
			'back':back,
			'ahead':ahead,
			'total':total,
			'virus':virus,
			'exit':exit,
			'blocked':False
		}


	def set_next_target():
		"""sorts known viruses based on taxicab distance from current node.

		inputs:
		targets: list of dicts. Contains all targets (viruses and exits).

		outputs:
		the targets list, sorted based on the distance of each point to
		the current location, based on taxicab geometry.

		"""
		global all_nodes, current_node, current_dir, goal, queue, targets
		global FORWARD, RIGHT, BACKWARD, LEFT

		if robot.num_viruses_left > 0:

			viruses = []

			for target in targets:
				if target['virus']:
					viruses.append(target)

			virus_dists = [(taxicab_dist(current_node['pos'], virus['pos']) \
				for virus in viruses)]

			return viruses[virus_dists.index(min(virus_dists))]

		else:	# no viruses remain, so an exit is chosen
			return targets[0]	# what exit is chosen is irrelevant


	def change_dir(direction):
		"""change robot orientation while maintaining robot.current_dir.
		
		inputs:
		current_dir: int in range(0,4) 
		direction: int in range(0,4)

		Both of the above must equal either FORWARD, RIGHT, BACKWARD, or LEFT.
		
		outputs: None
		
		direction is first sanity-tested. If it passes and the robot's current
		direction is alreads accurate, nothing happens. Otherwise, the
		current_dir variable is changed to ensure that current_dir
		is still in range(0,4), and then the robot is reoriented with
		robot.turn_left or robot.turn_right, depending on whether 
		direction > current_dir or direction < current_dir.

		Not fully optimal (a left turn while facing FORWARD requires 3 calls of
		the robot.turn_right() method, for instance), but this implementation
		simplifies code and increases readability.
		
		"""
		global all_nodes, current_node, current_dir, goal, queue, targets
		global FORWARD, RIGHT, BACKWARD, LEFT
		
		if direction in range(0,4):
			if current_dir == direction:
				pass
			elif current_dir < direction:
				difference = direction - current_dir
				robot.turn_right(difference)
				current_dir += difference
			elif current_dir > direction:
				difference = current_dir - direction
				robot.turn_left(difference)
				current_dir -= difference
		else:
			raise ValueError("direction must be FORWARD (0), "
				"RIGHT (1), BACKWARD (2), or LEFT (3)")


	def find_dir(node):
		"""return the direction of movement required to reach a point

		inputs:
		node: dict. containing the coordinates of a point

		outputs:
		direction: either FORWARD, BACKWARD, LEFT, or RIGHT

		This function is an intermediary which will reduce code reuse for
		determining how to proceed and how to move in the graph.

		"""
		global all_nodes, current_node, current_dir, goal, queue, targets
		global FORWARD, RIGHT, BACKWARD, LEFT

		x = current_node['pos'][0]
		y = current_node['pos'][1]

		if node['pos'] == (x, y + 1):
			return FORWARD
		elif node['pos'] == (x, y - 1):
			return BACKWARD
		elif node['pos'] == (x + 1, y):
			return RIGHT
		else:
			return LEFT


	def find_node(point=None):
		"""determine if a particular node exists, returns node if possible.

		inputs:
		point: a 2-tuple defining coordinates. By default, this attribute is
		set to None, meaning that find_node() will use whatever square is
		one ahead of the robot as the Node to search for.
		If point is specified, find_node() will search for a node with a
		pos attribute equal to point.

		outputs:
		other_node: node. The desired node object.
		None: if point is not specified and there is no square one ahead of
		the robot, None is returned. This is useful for conditional statements
		in other methods and functions (see: set_virus_points(),
		set_exit_points(), set_neighborhood(), or move()).

		"""
		global all_nodes, current_node, current_dir, goal, queue, targets
		global FORWARD, RIGHT, BACKWARD, LEFT

		pos = current_node['pos']


		if point != None:		# A specific point is given (exit, virus, etc.)
			node_found = False
			for other_node in all_nodes:
				if other_node['pos'] == point:
					node_found = True
					return other_node

			if not node_found:
				node_there = make_node(point)
				all_nodes.append(node_there)
				return node_there


		elif robot.sense_steps(robot.SENSOR_FORWARD) > 0:
			if current_dir == FORWARD:
				node_found = False
				for other_node in all_nodes:
					if other_node['pos'] == (pos[0], pos[1] + 1):
						node_found = True
						return other_node
				
				if not node_found:
					node_there = make_node((pos[0], pos[1] + 1))
					all_nodes.append(node_there)
					return node_there

			elif current_dir == RIGHT:
				node_found = False
				for other_node in all_nodes:
					if other_node['pos'] == (pos[0] + 1, pos[1]):
						node_found = True
						return other_node
				
				if not node_found:
					node_there = make_node((pos[0] + 1, pos[1]))
					all_nodes.append(node_there)
					return node_there

			elif current_dir == BACKWARD:
				node_found = False
				for other_node in all_nodes:
					if other_node['pos'] == (pos[0], pos[1] - 1):
						node_found = True
						return other_node
				
				if not node_found:
					node_there = make_node((pos[0], pos[1] - 1))
					all_nodes.append(node_there)
					return node_there

			elif current_dir == LEFT:
				node_found = False
				for other_node in all_nodes:
					if other_node['pos'] == (pos[0] - 1, pos[1]):
						node_found = True
						return other_node

				if not node_found:
					node_there = make_node((pos[0] - 1, pos[1]))
					all_nodes.append(node_there)
					return node_there

			else:
				return None 	# No node in specified area


	def set_virus_points():
		"""compile all viruses in 10-sq radius, with objective coordinates.

		inputs: None

		outputs: None
		Viruses not previously known will be appended to target_list

		"""
		global all_nodes, current_node, current_dir, goal, queue, targets
		global FORWARD, RIGHT, BACKWARD, LEFT

		pos = current_node['pos']

		for virus in robot.sense_viruses():
			virus_node = find_node(point=\
				(virus[0] + pos[0], virus[1] + virus[1])\
				)
			virus_node['virus'] = True
			if virus_node not in targets:
				targets.append(virus_node)
			if virus_node not in all_nodes:
				all_nodes.append(virus_node)


	def set_neighborhood():
		"""hi there, neighbor. This defines a node's neighborhood.

		inputs: None (you don't need to give me anything but love)

		outputs:
		a beautiful list of neighbors for the current node

		***NOTE: THIS FUNCTION ALSO ADDS THE INPUT NODE TO THE OTHER NODE'S
		neighborhood, IF NOT ALREADY PRESENT THERE***

		***EASTER_EGG_NOTE: THE AUTHORS OF THIS CODE REFERENCE 
		"Mr. Roger's Neighborhood", A CHILDHOOD FAVORITE. 
								RIP 
							FRED ROGERS
							B: 3/20/1928
							D: 2/27/2003***

		"""
		global all_nodes, current_node, current_dir, goal, queue, targets
		global FORWARD, RIGHT, BACKWARD, LEFT

		current_node['neighborhood'] = []
		
		proper_dir = current_dir
		num_turns = 0

		while num_turns < 4:
			node_ahead = find_node()
			if node_ahead != None:
				if node_ahead['pos'] not in current_node['neighborhood']:
					# it's a beautiful day
					current_node['neighborhood'].append(node_ahead['pos'])

			change_dir((current_dir + 1) % 4)
			num_turns += 1

		for neighbor in current_node['neighborhood']:
			neighbor_node = find_node(neighbor)
			if current_node['pos'] not in neighbor_node['neighborhood']:
				neighbor_node['neighborhood'].append(current_node['pos'])


	def move(direction, num_steps=1, backtracking=False):
		"""wrapper function for robot.step_forward().
		
		inputs: 
		direction: int in range(0,4)
			Must equal either FORWARD, LEFT, BACKWARD, or RIGHT
		num_steps: int, as in robot.step_forward()
		backtracking: boolean.
		
		outputs: None
		
		Robot orients itself to face direction, then moves one step at a time,
		maintaining current position and a list of all nodes as it goes.
		The main purpose for this is to ensure that virus sweeping can be
		constant. With each step, the set_virus_points() function, a wrapper
		for robot.sense_viruses(), is called, increasing the likelihood that
		any and all viruses that can be found will be found as quickly as 
		possible.
		
		Unfortunately, the insistence on rigorous sweeping and location
		maintenance decreases the speed of this method. Further testing must be
		undergone to determine the value-to-cost ratio of this particular
		trade-off.
		
		"""
		global all_nodes, current_node, current_dir, goal, queue, targets
		global FORWARD, RIGHT, BACKWARD, LEFT

		change_dir(direction)
		
		steps_taken = 0
		while steps_taken < num_steps:
			next_step = find_node()
			if next_step != None:
				robot.step_forward()

				if not backtracking:
					next_step['parent'] = current_node['pos']
				
				current_node = next_step
				set_neighborhood()

			
			# constant sweeping ensures that no viruses are missed.
			set_virus_points()
			steps_taken += 1


	def rhs(node):
		"""follows definition of rhs-value, as given in D* Lite paper.

		inputs:
		node: a node dict.

		outputs:
		min_dist: the minimum sum of a successor node's forward cost and the
		cost between the successor node and the input node.

		***NOTE: Remember, this is backwards 
		(heading toward goal, not current_node) because in the D* pseudocode,
		the goal has all costs of 0, and you go towards lessening costs.***

		"""
		global all_nodes, current_node, current_dir, goal, queue, targets
		global FORWARD, RIGHT, BACKWARD, LEFT

		if node == goal:
			return 0
		else:
			min_dist = float('inf')
			for other in node['successors']:
				other_node = find_node(other)
				distance = other_node['ahead'] + taxicab_dist(other, node['pos'])
				if distance < min_dist:
					min_dist = distance

			return min_dist


	def update_vertex(node):
		"""compares node 'ahead' value to rhs-value while maintaining the queue.

		inputs:
		node: a node dict.

		outputs: None

		"""
		global all_nodes, current_node, current_dir, goal, queue, targets
		global FORWARD, RIGHT, BACKWARD, LEFT

		if node in queue:
			queue.remove(node)
		if rhs(node) != node['ahead']:
			node['ahead'] = rhs(node)
			node['total'] = min([rhs(node), node['ahead']]) + node['back']
			queue.append(node)


	def update_node(node):
		"""updates key node attributes based on current information

		inputs:
		node: a node dict

		outputs: None

		The input node will be modified only if its attributes changed.
		This makes this method safe.

		"""
		global all_nodes, current_node, current_dir, goal, queue, targets
		global FORWARD, RIGHT, BACKWARD, LEFT

		node['back'] = taxicab_dist(node['pos'], current_node['pos'])
		node['ahead'] = taxicab_dist(node['pos'], goal['pos'])
		node['total'] = node['back'] + node['ahead']

		node['predecessors'] = []
		node['successors'] = []

		for pos in node['neighborhood']:
			if taxicab_dist(pos, goal['pos']) < node['ahead']:
				node['successors'].append(pos)
			else:
				node['predecessors'].append(pos)


	def shortest_path():
		"""modifies surrounding nodes to unveil the shortest path.

		inputs: None

		outputs: None

		"""
		global all_nodes, current_node, current_dir, goal, queue, targets
		global FORWARD, RIGHT, BACKWARD, LEFT
		
		queue = sorted(queue, key=lambda x: x['ahead'])
		while queue[0]['ahead'] < current_node['ahead'] or \
			rhs(current_node) != current_node['ahead']:
			queue = sorted(queue, key=lambda x: x['ahead'])
			next = queue.pop(0)
			if next['ahead'] >= rhs(next):
				next['ahead'] = rhs(next)
				for pos in next['predecessors']:
					node = find_node(pos)
					update_vertex(node)
			else:
				if next != goal:
					next['ahead'] = float('inf')
				for pos in next['predecessors']:
					node = find_node(pos)
					update_vertex(node)
			if len(queue) == 0:
				return None

	
	def backtrack():
		"""move to a node that presents an opportunity for progress.

		inputs: None

		outputs: None

		"""
		global all_nodes, current_node, current_dir, goal, queue, targets
		global FORWARD, RIGHT, BACKWARD, LEFT

		btvisited = []

		neighbors = [find_node(n) for n in current_node['neighborhood']]

		# changed general generator statement to a list
		hope = any([n['ahead'] < float('inf') for n in neighbors])

		while not hope:
			if len(current_node['neighborhood']) == 1:
				if not current_node['blocked']:
					current_node['blocked'] = True
				if current_node not in btvisited:
					btvisited.append(current_node['pos'])
				change_dir((current_dir - 2) % 4)
				parent_node = find_node(current_node['parent'])
				move(find_dir(parent_node), backtracking=True)
			elif len(current_node['neighborhood']) == 2:
				if current_node not in btvisited:
					btvisited.append(current_node['pos'])
				any_options = False
				for neighbor in current_node['neighborhood']:
					neighbor_node = find_node(neighbor)
					if not neighbor_node['blocked']:
						any_options = True
						move(find_dir(neighbor_node), backtracking=True)
				if not any_options:
					# this approach to the error is untested.
					while current_node['blocked'] and current_node['parent']:
						parent_node = find_node(current_node['parent'])
						if current_node not in btvisited:
							btvisited.append(current_node)
						move(find_dir(parent_node), backtracking=True)
					
					for node in btvisited:
						node['blocked'] = True

					for node in all_nodes:
						if not node['blocked']:
							pos = node['pos']
							node['ahead'] = taxicab_dist(pos, goal['pos'])
			else:
				opts = []
				for neighbor in current_node['neighborhood']:
					if not find_node(neighbor)['blocked']:
						opts.append(neighbor)
				if len(opts) > 0:
					goal_pos = goal['pos']
					opt_dists = [taxicab_dist(goal_pos, opt) for opt in opts]
					choice = find_node(opts[opt_dists.index(min(opt_dists))])
					if current_node not in btvisited:
						btvisited.append(current_node['pos'])
					move(find_dir(choice), backtracking=True)
				else:
					next = current_node['parent']
					if current_node not in btvisited:
						btvisited.append(current_node['pos'])
					move(find_dir(find_node(next)), backtracking=True)
			
			neighbors = [find_node(n) for n in \
				current_node['neighborhood']]

			hope = any([n['ahead'] < float('inf') for n in neighbors])

			update_vertex(current_node)
			for n in current_node['neighborhood']:
				update_vertex(find_node(n))

		if hope:
			for pos in btvisited:
				# temporarily blocks off all squares that cannot lead to goal
				find_node(pos)['blocked'] = True
			options = []
			for n in neighbors:
				if n['ahead'] < float('inf'):
					options.append(n)
			if len(options) == 1:
				move(find_dir(options[0]))
			else:
				min_dist = min([n['ahead'] for n in options])
				move(find_dir(options[options.index(min_dist)]))


	def backtrackv2():
		"""retreat along non-optimal paths in order to reach superior position

		inputs: None
		
		ouptuts: None

		"""
		global all_nodes, current_node, current_dir, goal, queue, targets
		global FORWARD, RIGHT, BACKWARD, LEFT

		btvisited = []

		neighbors = [find_node(n) for n in current_node['neighborhood']]
		hope = any([n['ahead'] < float('inf') for n in neighbors])

		while not hope:
			hood = current_node['neighborhood']
			btvisited.append(current_node['pos'])
			if len(hood) == 1:
				current_node['blocked'] = True
			elif len(hood) == 2:
				for pos in hood:
					if find_node(pos)['blocked']:
						current_node['blocked'] = True
			elif all([find_node(n)['blocked'] for n in hood]):
				current_node['blocked'] = True
			else:
				blocked_off = 0
				for pos in hood:
					if find_node(pos)['blocked']:
						blocked_off += 1
				if blocked_off == len(hood) - 1:
					current_node['blocked'] = True
			pred_nodes = [find_node(n) for n in current_node['predecessors']]
			pred_nodes = sorted(pred_nodes, key=lambda n: \
				taxicab_dist(n['pos'], goal['pos']))
			for pred in pred_nodes:
				if not pred['blocked']:
					move(find_dir(pred))
				
			neighbors = [find_node(n) for n in hood]
			hope = any([n['ahead'] < float('inf') for n in neighbors])

		for pos in btvisited:
			find_node(pos)['blocked'] = True

		if current_node == goal:
			targets.remove(current_node)
			for node in all_nodes:
				node['blocked'] = False
				update_node(node)



	# begin control code
	current_dir = FORWARD
	all_nodes = []
	
	minimum = float('inf')
	min_pos = None
	for n in robot.sense_viruses():
		if taxicab_dist(n, (0,0)) < minimum:
			minimum = taxicab_dist(n, (0,0))
			min_pos = int(n[0]), int(n[1])

	x = min_pos[0]
	y = min_pos[1]
	goal_hood = [(x,y+1), (x+1,y), (x,y-1), (x-1,y)]
	goal = {
		'pos':min_pos,
		'parent':None,
		'neighborhood':goal_hood,
		'predecessors':goal_hood,
		'successors':[],
		'back':(min_pos[0] + min_pos[1]),
		'ahead':0,
		'total':(min_pos[0] + min_pos[1]),
		'virus':True,
		'exit':False,
		'blocked':False}

	current_node = {
		'pos':(0,0),
		'parent':None,
		'neighborhood':[],
		'predecessors':[],
		'successors':[],
		'back':0,
		'ahead':taxicab_dist((0,0),goal['pos']),
		'total':taxicab_dist((0,0),goal['pos']),
		'virus':False,
		'exit':False,
		'blocked':False}

	set_neighborhood()
	queue = [goal,]

	all_nodes.append(current_node)
	all_nodes.append(goal)

	targets = [goal,]

	# this snippet of code rewritten because exit positions were being defined
	# 	as lists, not as tuples.
	for pos in robot.sense_exits():
		targets.append(make_node(pos, exit=True))

	set_virus_points()


	for neighbor in current_node['neighborhood']:
		if taxicab_dist(neighbor, goal['pos']) < current_node['ahead']:
			current_node['successors'].append(neighbor)
		else:
			current_node['predecessors'].append(neighbor)

	# set extreme points, as they are known initially
	min_x = min(node['pos'][0] for node in all_nodes)
	max_x = max(node['pos'][0] for node in all_nodes)
	min_y = min(node['pos'][1] for node in all_nodes)
	max_y = max(node['pos'][1] for node in all_nodes)
	all_x = range(int(min_x), int(max_x + 1))
	all_y = range(int(min_y), int(max_y + 1))

	nodes = []
	for x in all_x:
		for y in all_y:
			nodes.append(make_node((x,y)))

	for node in nodes:
		if node not in all_nodes:
			all_nodes.append(node)

	shortest_path()
	while True:
		if goal['exit'] and any((n['virus'] for n in targets)):
			# while searching for a virus, the robot aims for exit.
			# if virus is found, it should become the target
			set_next_target()
		if current_node['pos'] == goal['pos'] and current_node['virus']:
			# the robot is at the virus
			targets.remove(current_node)
			current_node['virus'] = False
			
			goal = set_next_target()

			queue = [goal,]

			for node in all_nodes:
				# should we be updating so often?
				# update methods theoretically safe, but time inefficient.
				update_node(node)
				update_vertex(node)
				# when looking for a new node, it is unknown whether blocked
				# 	paths will be useful, or will continue to be dead ends.
				node['blocked'] = False

		elif current_node in targets and current_node['virus']:
			# the robot moved on a virus that is was not aiming for.
			targets.remove(current_node)
			current_node['virus'] = False

		while current_node['pos'] != goal['pos']:
			if goal['exit'] and any((n['virus'] for n in targets)):
				# while searching for a virus, the robot aims for exit.
				# if virus is found, it should become the target
				set_next_target()
			if current_node['ahead'] == float('inf'):	# there is no path.
				backtrack()
			min_dist = float('inf')
			next_node = None
			for pos in current_node['successors']:
				dist = taxicab_dist(pos, goal['pos'])
				if dist < min_dist:
					if not find_node(pos)['blocked']:
						min_dist = dist
						next_node = find_node(pos)
			if next_node != None:
				move(find_dir(next_node))
				update_node(current_node)
			else:
				raise ValueError("No valid paths!")
			for node in all_nodes:
				update_node(node)
				update_vertex(node)
			if current_node['pos'] == goal['pos'] and current_node['virus']:
				# the robot is at the virus
				targets.remove(current_node)
				current_node['virus'] = False
				
				goal = set_next_target()

				queue = [goal,]

				for node in all_nodes:
					# should we be updating so often?
					# update methods theoretically safe, but time inefficient.
					update_node(node)
					update_vertex(node)
					# when looking for a new node, it is unknown whether blocked
					# 	paths will be useful, or will continue to be dead ends.
					node['blocked'] = False
			shortest_path()
		queue = sorted(queue, key=lambda x: x['ahead'])
		shortest_path()