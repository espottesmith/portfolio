# Node.py
# 
# An abstract class representing a node in a maze based on the relative 
#   starting position of the robot in search of a virus. 
# Since the Node class includes objective coordinates, it can be used even with
#       with algorithms that do not require manhattan heuristics or some other
#       aspect of this class.
#
#
# Written by: Marcus Fedarko
# Edited by: Evan Spotte-Smith

class Node(object):
    """represents a node: a single point in the maze.

    The Node class has built-in compatibility with all algorithms being used
    in this project (Wallfollower, Pledge, A*, D*, etc.). Not all attributes
    and methods will be used by each algorithm, but the capability exists.
    Under consideration is whether or not to make Node a base class which can
    be extended under other classes in specific controller files.

    attributes:
    g_cost: int. The cost of moving from start to this Node. Will not be
    utilized in all algorithms (ex: Wallfollower).

    h_cost: int. The projected cost of moving from this Node to the goal. In
    this implementation, h_cost uses the Manhattan heuristic, which is
    equivalent to the taxicab distance between two points. Will not be utilized
    in all algorithms (ex: Wallfollower).

    f_cost: int. The total estimated cost of moving from start to the goal
    through this point, based on g_cost and h_costWill not be utilized in all
    algorithms (ex: Wallfollower).

    Other attributes discussed in __init__() (see below).

    """


    def __init__(self, pos, neighborhood=[], parent=None, goal=None, \
                    start=False):
        """initializes the node.
        
        inputs:
        pos: 2-tuple. a tuple of the form (x, y) detailing the node's position
        in an objective coordinate scheme, with (0,0) being the starting point.

        neighborhood: list of Nodes. a list of all adjascent Nodes to this
        Node. This defaults to an empty list, but as the robot goes through
        different nodes, it defines the neighbors of all of the nodes it
        passes.

        parent: Node. the "parent" Node - the direct predecessor. If parent is
        left as None, then the g_cost for this node will default to infinity,
        unless this is the starting Node.

        goal: Node. the goal Node. This defaults to None because some
        algorithms, such as Wallfollower or Pledge, do not aim for particular
        goals, but essentially wander. It is thus pointless and illogical to force
        such algorithms to implement a goal scheme.

        start: Boolean. Indicates whether or not this Node is the
        starting Node. By default, this will be False, but the instantiation
        method of any algorithmic implementation will set (0,0) to be start

        outputs: None

        """

        self.pos = pos
        self.neighborhood = neighborhood    # will be filled when needed
        self.parent = parent
        self.goal = goal

        self.rhs = 0    # right-hand-side distance will be determined later


        if self.parent:     # equivalent to if self.parent != None:
            self.g_cost = self.parent.g_cost + 1
            self.predecessors = self.parent.predecessors
            self.predecessors.append(self.parent)
        elif start and not self.parent:
            self.g_cost = 0
            self.predecessors = []
        else:
            self.g_cost = float("inf")  # until Node encountered, cost infinite


        if self.goal:
            self.h_cost = self.find_taxicab_dist(self.goal)
        else:
        	self.h_cost = 0    # heuristic = 0 until goal is defined

        self.f_cost = self.g_cost + self.h_cost # ignore for some algorithms


    def change_parent(self, new_parent):
        """changes the parent node of this node.

        inputs:
        self: this Node object
        new_parent: a Node with which to replace self.parent

        outputs: None

        Automatically recalculate this Node's g_cost, f_cost, predecessor list.

        """

        self.parent = new_parent
        self.g_cost = self.parent.g_cost + 1
        self.predecessors = self.parent.predecessors
        self.predecessors.append(self.parent)


    def change_goal(self, new_goal):
        """changes the goal Node of this Node.

        inputs:
        self: this Node object
        new_goal: a Node with which to replace self.goal

        outputs: None

        Automatically recalculates this Node's h_cost and f_cost.

        """

        self.goal = new_goal
        self.h_cost = self.find_taxicab_dist(self.goal)


    def calculate_key(self):
        """recalculates f_cost and g_cost based on rhs

        inputs:
        self: the Node object

        outputs: None

        """

        self.g_cost = min(self.g_cost, self.rhs)
        self.f_cost = self.g_cost + self.h_cost


    def find_taxicab_dist(self, goal):
        """standard implementation of two-dimensional taxicab distance formula

        inputs:
        self: the Node object
        goal: Node. the goal point to which this point is being compared.

        outputs:
        output of two-dimensional distance formula in taxicab geometry:
            d = (x_2 - x_1) + (y_2 - y_1)
        with x_1 and y_1 originating from this node's location.

        In taxicab geometry, a discrete, non-continuous geometric system,
        points are defined unconventionally: rather than having points at all
        real numbers, there are only points at every integral value, ie -1, 0,
        1, etc. As such, a straight like can no longer be defined as the
        shortest distance between two points, as often, one cannot make a
        straight line between two points (like between (0,0) and (1,1), in
        which the optimal path either goes through (0,1) or through (1,0)).
        This function was chosen, as opposed to the traditional distance
        formula sqrt((x_2 - x_1)**2 + (y_2 - y_1)**2), because the robot cannot
        move diagonally, but can only go FORWARD, RIGHT, BACKWARD, or LEFT.

        ***NOTE: THIS IS THE SAME THING AS A MANHATTAN HEURISTIC FUNCTION.***

        Does not necessarily produce the actual distance or cost from a start
        point to the goal.

        """

        return abs(goal.pos[0] - self.pos[0]) + abs(goal.pos[1] - self.pos[1])