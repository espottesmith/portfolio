# smart_pledge.py
# 
# A module designed to move the robot according to an "enhanced" version of
#   the Pledge Algorithm.
# Smart-Pledge is basically just "normal" Pledge (as seen in pledge.py), but
#   with one variation: the goal direction is set by the location of the
#   closest virus to the robot.
# If the robot's sensors cannot detect any viruses nearby, activated exits
#   are tested for and then used as the next "goal" of the algorithm.
#
# Written by: Marcus Fedarko, with suggestions from Kai Atanasoff and
# Evan Spotte-Smith
#
# TODO:
#   -Refactor to use the stuff in base.py (especially for
#    get_goal_direction())

from math import sqrt

# how we count turns while wall-following
LEFT_TURN = -1
RIGHT_TURN = 1

# absolute (from the robot's starting point) directions
FORWARD = 0
RIGHT = 1
BACKWARD = 2
LEFT = 3

def wall_follow(robot, goal_orient):
	"""wallfollows RIGHT until the robot is facing the desired orientation
	and the robot's "turn count" is equal to zero.
    
	inputs:
	robot: the robot object
    goal_orient: The desired end orientation of the robot. Must be equal
	to FORWARD, RIGHT, BACKWARD, or LEFT.

	outputs: None

	"""

	turn_count = 0

	while True:
		if robot.sense_steps(robot.SENSOR_RIGHT) >= 1:
			robot.turn_right()
			robot.step_forward()
			turn_count += RIGHT_TURN
			
		elif robot.sense_steps(robot.SENSOR_FORWARD) >= 1:
			robot.step_forward()
			
		elif robot.sense_steps(robot.SENSOR_LEFT) >= 1:
			robot.turn_left()
			robot.step_forward()
			turn_count += LEFT_TURN
		else:
			# we've got to backtrack
			robot.turn_left(2)
			turn_count += (LEFT_TURN * 2)

def get_goal_direction(robot, default_direction):
	"""determines the goal direction for the robot to go in, based on the
	location of any viruses or active exits near the robot. this uses the
	taxicab distance metric for estimating the 'distance' from the robot to
	a virus/exit.
	
	inputs:
	robot: the robot object
	default_direction: if there are viruses left (but none within range of
	the robot's sensor), the robot will go in this direction. (if there are
	no viruses left, the robot will head towards the closest exit.)	
	
	outputs:
	direction: one of FORWARD, RIGHT, BACKWARD, or LEFT
	
	"""
	
	viruses = robot.sense_viruses()
	# closest_virus: the relative [x, y] coordinate of the virus closest to
	# the robot
	closest_virus = None
	# closest_virus_dist: the distance between closest_virus and the robot
	closest_virus_dist = None
	
	for v in viruses:
		dist = sqrt((v[0])**2 + (v[1])**2)
		if (closest_virus_dist == None) or (dist < closest_virus_dist):
			closest_virus = v
			closest_virus_dist = dist
			
	if closest_virus == None:
		# no viruses located
		if robot.num_viruses_left() > 0:
			# there are viruses left, just none near the robot. Go according
			# to the default direction.
			return default_direction
		else:
			# no viruses left. find exits now
			# TODO: this reuses the virus-handling procedure earlier in this
			# function. mitigate the code reuse as a result.
			exits = robot.sense_exits()
			closest_exit = None
			closest_exit_dist = None
			for e in exits:
				dist = sqrt((e[0])**2 + (e[1])**2)
				if (closest_exit_dist == None) or (dist < closest_exit_dist):
					closest_exit = e
					closest_exit_dist = dist
			if closest_exit == None:
				# something went wrong. there isn't an exit in the level? this
				# shouldn't happen.
				print "ERROR: no exits found. Goal direction set to default."
				return default_direction
			else:
				if abs(closest_exit[0]) < abs(closest_exit[1]):
					if closest_exit[0] > 0: return RIGHT
					else: return LEFT
				else:
					if closest_exit[1] > 0: return FORWARD
					else: return BACKWARD
	# TODO: mitigate code reuse here. this does not look very readable now.
	else:
		if abs(closest_virus[0]) < abs(closest_virus[1]):
			if closest_virus[0] > 0: return RIGHT
			else: return LEFT
		else:
			if closest_virus[1] > 0: return FORWARD
			else: return BACKWARD
			
def control_robot(robot):
	"""uses the Smart-Pledge Algorithm to walk the robot through the maze.
	this should terminate when the robot reaches the exit.
	
	inputs:
	robot: the robot object
	
	outputs: None
	
	"""

	while True:
		GOAL_ORIENT = get_goal_direction(robot, FORWARD)
		
		if GOAL_ORIENT == RIGHT:
			robot.turn_right()
		elif GOAL_ORIENT == LEFT:
			robot.turn_left()
		elif GOAL_ORIENT == BACKWARD:
			robot.turn_left(2)
		fwd_space = robot.sense_steps(robot.SENSOR_FORWARD)
		
		if fwd_space > 0:
			robot.step_forward(fwd_space)
		else:
			# we've hit an island
			# wall_follow() will last until the robot "escapes" the island
			wall_follow(robot, GOAL_ORIENT)