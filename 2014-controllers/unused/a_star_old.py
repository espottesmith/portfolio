"""

a_star_old.py

STUFF ABOUT THE ROBOT:

=================
robot.orient: The direction the robot is facing, relative to how it's facing
originally. Represented by a number 0-3: 0 is north, 1 is east, 2 is south, 3 is
west. Illustrated by the diagram below.
  0
3 R 1
  2
We need to keep orient updated; otherwise, the values returned by sense_viruses() will be
useless since we won't know which direction to go in.
=================

HOW THIS WORKS:

-The robot uses a variant of A* in tandem with robot.sense_viruses().

-Otherwise it uses a wall follower until it can sense viruses again.


NOTE:
Code lines marked with a #! have been changed from what they were in submitted_controller.
Memorize changes!

Written by: Marcus Fedarko
(I apologize in advance for the old formatting style. This was the fruit of my efforts last year - as you can see, it didn't work.)
"""

import math
import Node

OVERRIDE_INITIAL_GOAL = None #!

def distance(p1, p2):
    """Finds the straight-line distance between two points of the form (x, y)."""

    d = math.sqrt(((p1[0] - p2[0]) ** 2) + ((p1[1] - p2[1]) ** 2))
    return d

def reset_orient(robot):
    """Resets the orientation of the robot to 0."""

    # Make sure robot is facing north in order to get accurate, understandable
    # readings from robot.sense_viruses() and do other stuff. Really, we could compensate
    # for changes in orientation but it'd be way too annoying to write and only cost a bit less time.
    if robot.orient == 1:
        robot.turn_left()
    elif robot.orient == 2:
        robot.turn_left(2)
    elif robot.orient == 3:
        robot.turn_right()
    robot.orient = 0

def spaces_in_4_directions(robot):
    """Finds the steps remaining until the next wall for each of the cardinal directions.
    Returns a tuple of the form (N, W, E, S)."""

    reset_orient(robot)

    rgt = robot.sense_steps(robot.SENSOR_RIGHT)
    fwd = robot.sense_steps(robot.SENSOR_FORWARD)
    lft = robot.sense_steps(robot.SENSOR_LEFT)
    robot.turn_left(1)
    bck = robot.sense_steps(robot.SENSOR_LEFT)
    robot.turn_right(1)

    return fwd, lft, rgt, bck

def move_to_adjacent_node(robot, current_node, next_node):
    """Moves the robot to an adjacent node (a node exactly one square away from the robot on either
    the x- or y- axis)."""
    
    reset_orient(robot)
    if next_node.pos[0] > current_node.pos[0]:
        robot.turn_right()
        robot.step_forward()
        robot.orient = 1

    elif next_node.pos[0] < current_node.pos[0]:
        robot.turn_left()
        robot.step_forward()
        robot.orient = 3

    elif next_node.pos[1] > current_node.pos[1]:
        robot.step_forward()

    elif next_node.pos[1] < current_node.pos[1]:
        robot.turn_left(2)
        robot.step_forward()
        robot.orient = 2
    
def control_robot(robot):

    robot.orient = 0 # see above spiel in docstring re: orientation and its significance
    times_looped = 0 #!
    while True:
        #NOTE This basically lets us set a goal position if there aren't any viruses detectable from start.
        # only implement if we need it on challenge day.
        if (times_looped == 0) and (OVERRIDE_INITIAL_GOAL != None): #!
            goal_virus_pos = OVERRIDE_INITIAL_GOAL #!
        else: #!
            reset_orient(robot) #!
            #print "Sensing for viruses."
            nv = robot.sense_viruses()
            #print "Sensed viruses at: [",
            #for a in nv:
                #print "(%d, %d)" % (a[0], a[1]),
            #print "]"
            
            goal_virus_pos = None
            for v in nv:
                # NOTE Alright, for whatever reason, robot.sense_viruses() keeps on returning (0, 0) as a virus that exists even if
                # it's been destroyed. Not sure what causes this bug but this'll circumvent it. It is EXTREMELLY IMPORTANT to change this
                # on challenge day. NOTE This really means that the people who made this framework screwed up. Shove it in their unsuspecting
                # faces come challenge day.
                if v[0] == 0 and v[1] == 0: #!
                    #print "Sensed the current position as a virus. lolwut." #!
                    continue #!
                # we use (0, 0) because according to robot.sense_viruses(), the origin is where we are now.
                d = distance((0, 0), v)
                #print "Detected virus. Distance to it from current position: %g" % (d)
                if goal_virus_pos == None:
                    goal_virus_pos = v
                    #print "Set virus to temporary goal. No prior past goal."
                    continue
                elif distance((0, 0), goal_virus_pos) > d:
                    #print "Set virus to temporary goal. Prior past goal position: (%d, %d)" % (goal_virus_pos[0], goal_virus_pos[1])
                    goal_virus_pos = v

        # Now we should (keyword here is should) have the relative location of
        # the closest virus to the robot (within 10 spaces, obviously).

        # What if no viruses were sensed by robot.sense_viruses()?
        # (NOTE: If there are no viruses left on the level, the robot will just wallfollow
        # forever. It should be obvious whether or not all of them have been found based on the point total
        # for that level.)
        if goal_virus_pos == None:
            # Wallfollow until detecting a virus.
            while True:
                if robot.sense_steps(robot.SENSOR_LEFT) >= 1:
                    robot.turn_left()
                    robot.step_forward()
                elif robot.sense_steps(robot.SENSOR_FORWARD) >= 1:
                    robot.step_forward()
                elif robot.sense_steps(robot.SENSOR_RIGHT) >= 1:
                    robot.turn_right()
                    robot.step_forward()
                else:
                    robot.turn_right(2)
                if len(robot.sense_viruses()) > 0:
                    break
            # Since we've stopped wall-following, we've detected a virus.
            continue
        # Okay now we KNOW we have a virus closest to the robot, and we know its relative position.
        # Use A*!

        # ============================== Begin A* ================================
        current_node = Node.Node((0, 0), goal_virus_pos)
        open_list = [current_node]
        closed_list = []

        while True:
            #if goal_virus_pos[0] == 0 and goal_virus_pos[1] == -1:
                #print "At position (%d, %d)." % (current_node.pos[0], current_node.pos[1])
                #print "Open list:",
                #for o123 in open_list:
                    #print o123,
                #print "\nClosed list:"
                #for c123 in closed_list:
                    #print c123,
                #print "\n"
            ########### BELOW IS DOCUMENTATION STUFF, COMMMENT OUT FOR CHALLENGE/ETC ###########
            #print "Going through A* logic mainloop. Current position is (%d, %d)" % (current_node.pos[0], current_node.pos[1]),
            #print "open_list right now:"
            #for o in open_list:
                #if o.parent != None:
                    #print "(pos:", o.pos, "f:", o.f_cost, "g:", o.g_cost, "h:", o.h_cost, o.parent.pos, ")"
                #else:
                    #print "(pos:", o.pos, "f:", o.f_cost, "g:", o.g_cost, "h:", o.h_cost, "No parent", ")"

            #print "closed_list right now:"
            #for c in closed_list:
                #if c.parent != None:
                    #print "(pos:", c.pos, c.f_cost, c.parent.pos, ")"
                #else:
                    #print "(pos:", c.pos, c.f_cost, "No parent", ")"               
            ############################ END OF DOCUMENTATION STUFF ############################
            
            if closed_list == []:
                next_node = current_node
            else:
                next_node = None
                open_list.sort(key=(lambda node: node.f_cost))
                for m in open_list:
                    if m.pos[0] == current_node.pos[0]:
                        if abs(m.pos[1] - current_node.pos[1]) == 1:
                            next_node = m
                            break
                    elif m.pos[1] == current_node.pos[1]:
                        if abs(m.pos[0] - current_node.pos[0]) == 1:
                            next_node = m
                            break
                if next_node == None:
                    # Go to parent and re-test for more possible nodes.
                    # TODO Fix backtracking!!! Look at this: http://www.cs.bu.edu/teaching/alg/maze/
                    # Evidently, unmark nodes that we have to go to due to not being able to go anywhere else.
                    move_to_adjacent_node(robot, current_node, current_node.parent)
                    current_node = current_node.parent #!
                    continue

            open_list.remove(next_node)
            closed_list.append(next_node)
                
            move_to_adjacent_node(robot, current_node, next_node)
            #print "Current node: (%d, %d); Goal node: (%d, %d)" % (next_node.pos[0], next_node.pos[1], goal_virus_pos[0], goal_virus_pos[1])
            if (next_node.pos[0] == goal_virus_pos[0]) and (next_node.pos[1] == goal_virus_pos[1]):
                print "Got to virus at position (%d, %d)." % (next_node.pos[0], next_node.pos[1])
                break
            current_node = next_node
            
            # Sense possible areas of movement
            fwd, lft, rgt, bck = spaces_in_4_directions(robot)
            counter = 0
            #if current_node.parent.pos[0] != current_node.pos[0]:
                #if current_node.parent.pos[0] > current_node.pos[0]:
            # TODO There's definitely stuff to change here.        
            for direction in (fwd, lft, rgt, bck):
                if direction > 0:
                    
                    if counter == 0:
                        # There's at least 1 accessible node to the direct relative north.
                        new_pos = (current_node.pos[0], current_node.pos[1] + 1)
                    elif counter == 1:
                        # >= 1 accessible node to the relative west.
                        new_pos = (current_node.pos[0] - 1, current_node.pos[1])
                    elif counter == 2:
                        # >= 1 accessible node to the relative east.
                        new_pos = (current_node.pos[0] + 1, current_node.pos[1])
                    elif counter == 3:
                        # >= 1 accessible node to the relative south.
                        new_pos = (current_node.pos[0], current_node.pos[1] - 1)
                        
                    on_open_list = False                        
                    for o in open_list:
                        if o.pos == new_pos:
                            on_open_list = True
                            break

                    if not on_open_list:
                        on_closed_list = False
                        for c in closed_list:
                            if c.pos == new_pos:
                                on_closed_list = True
                                break
                        if not on_closed_list:
                            open_list.append(Node.Node(new_pos, goal_virus_pos, parent=current_node))
                    # NOTE Yeah, I don't think the below code is important for the purposes of this. We don't need to bother with
                    # any shortest-path stuff since this is all quick-and-dirty repetitive uses of A*.
                    #else:
                        ## On open list
                        #print "Found a node on the open list at position (%d, %d)! What to do?" % (o.pos[0], o.pos[1])
                        #if o.g_cost > (current_node.g_cost + 1):
                            #print "Changing parent of node at (%d, %d) to current node at (%d, %d)." % (o.pos[0], o.pos[1], current_node.pos[0], current_node.pos[1])
                            #o.change_parent(current_node)
                counter += 1
        times_looped += 1 #!