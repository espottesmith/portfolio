Projectile: Homework Two Problem 2
==================================

NOTE:
-----
I did attempt the extra credit for this problem. Additionally, my projectile
functions were based on answers given in Piazza, namely that s_sim must be able
to account for all delta_t, not just delta_t that divide evenly into t, but that
s_sim_multiple (the function designed for extra credit) could be designed to
work only with evenly divisible delta_t's.

Who wrote it?
-------------
Evan Spotte-Smith
ews2122

What is it?
-----------
This program solves a differential equation for the height of a projectile
launched vertically with constant and variable gravity. It also includes tester
files so that the accuracy and precision of the programs can be verified.

What does it contain?
---------------------
Three files in this directory contain programs relating to this problem:
projectile.py, projectile_tester.py, and projectile_tester_alternate.py.

What does it do?
----------------
projectile.py is a module containing a number of functions for calculating the
height of a projectile. s_next and v_next calculate the future position and
velocity, respectively, of a particle based on that particle's current position,
velocity, and a time interval (to determine how far in the future the program
should estimate). v_next uses another function, g, to estimate the force of
gravity at a particular height. s_sim takes an initial position and velocity,
as well as a final time and a time interval delta_t, to calculate the position
at time t; it does so by repeatedly using s_next and v_next. s_sim_multiple
works similarly to s_sim, but it returns a list of positions in one-second
increments, rather than a single, final position. It is also slightly
simplified, as it assumes that delta_t will divide evenly into t. Finally,
s_standard returns the position of the particle at time t with constant gravity.

Two tester files are included with projectile.py. projectile_tester.py is the
original, unmodified tester program, which uses s_sim and s_standard to generate
a particle's position at one-second increments based on some arbitrary initial
conditions. A modified version of this program,
projectile_tester_alternative.py, makes use of s_sim_multiple instead of s_sim.
This solution is more efficient because it ensures that no calculations are
duplicated. In the original tester, s_sim was run for t=0, t=1, t=2, etc. Each
time s_sim was called, the algorithm would compute the height at all previous
times. When s_sim was called with t=2, the time for t=1 would also be
calculated, which means that by the end of the program's execution, some
calculations were repeated hundreds or thousands of times.
projectile_tester_alternative.py avoids this efficiency pitfall by storing all
positions in a list which is then iterated over. Each position until the
particle hits the ground is calculated once and only once, and then that
information is displayed and compared with that from s_standard.

How was it designed?
--------------------
Much of the design for this program was handed to the authors - all of the
functions in projectile.py were laid out in the problem description, and
projectile_tester_alternate.py uses the same general structure as
projectile_tester.py.

One interesting design choice was to place a limitation on s_sim_multiple that
the position must remain greater than or equal to the start position. This goes
against the design choice made in projectile_tester.py, which was for
value-checking to occur in the tester, rather than in projectile.py. This choice
was made to simplify the design of projectile_tester_alternate.py. Because
s_sim_multiple stops calculation when it reaches a negative height, it can be
called with an arbitrarily large value for t without any risk of unnecessary
calculation. This seemed a simpler option than creating a generator function
which would be called until it yielded a negative value.

What can be concluded?
----------------------
This experiment with three forms of implementation, s_sim, s_standard, and
s_sim_multiple, shows how a change in one's assumptions can radically alter an
algorithm's output. While all implementations are mere approximations of the
projectile's actual path, both s_sim and s_sim_multiple provide boosts to
accuracy - s_standard consistently overestimates the force of gravity (because
it assumes that the projectile is always on the surface of the Earth), and thus,
its estimates of the projectile's height is consistently lower than those of
s_sim and s_sim_multiple.

Comparing projectile_tester.py and projectile_tester_alternate.py, one can see
how slight design modifications can produce significant gains in efficiency.
In this case, storing intermediate values ensures that calculations do not need
to be repeated (though this storage does increase the space complexity of the
algorithm).

How does it run?
----------------
To test projectile.py, alter the initial conditions in either
projectile_tester.py or projectile_tester_alternate.py and then run:
    python projectile_tester.py
or
    python projectile_tester_alternate.py
in a terminal window.
