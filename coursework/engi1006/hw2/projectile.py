#==============================================================================
# HW 2 - problem 2
#
# Evan Spotte-Smith
# 09/28/15
# File: projectile.py
#
# This uses numerical approximations to solve the projectile motion equation
# with varying gravity
#==============================================================================


import fractions as f


def g(h):
	"""returns gravitational acceleration at a given height.

	inputs:
	h - float. The height of the projectile

	outputs:
	float. The gravitational acceleration
	"""

	# define relevant constants
	GRAV_CONST = 6.6742 * 10**(-11)
	MASS_EARTH = 5.9736 * 10**(24)
	RADIUS_EARTH = 6371000

	return (GRAV_CONST * MASS_EARTH) / ((RADIUS_EARTH + h)**2)


def s_next(v_current, s_current, delta_t):
	"""returns approximation of position based on current conditions.

	inputs:
	v_current - float. Current velocity
	s_current - float. Current position
	delta_t - float. Time interval for calculation

	outputs:
	float. The estimated position for the next time interval
	"""

	return s_current + v_current * delta_t


def v_next(v_current, s_current, delta_t):
	"""returns approximation of velocity based on current conditions.

	inputs:
	v_current - float. Current velocity
	s_current - float. Current position
	delta_t - float. Time interval for calculation

	outputs:
	float. The estimated velocity for the next time interval
	"""

	return v_current - g(s_current) * delta_t


def s_sim(t, v_init, s_init, delta_t):
	"""returns estimation of projectile's position at a final time t.

	inputs:
	t - float. Final time
	v_init - float. Initial velocity
	s_init - float. Initial position
	delta_t - float. Time interval for estimations

	outputs:
	s_current - float. Final position of projectile at time t
	"""

	v_current = v_init
	s_current = s_init

	# t = (iterations)*delta_t + remainder_t
	# find iterations and remainder_t
	iterations = int(t/delta_t)
	remainder_t = t - (iterations * delta_t)

	# find position after all iterations
	n = 0
	while n < iterations:
		s_current = s_next(v_current, s_current, delta_t)
		v_current = v_next(v_current, s_current, delta_t)

		n += 1

	# find position after final time interval remainder_t
	s_current = s_next(v_current, s_current, remainder_t)

	return s_current


def s_standard(t, v_init):
	"""returns estimation of projectile's position after t seconds assuming
		constant gravity.

	inputs:
	t - float. Final time
	v_init - float. Initial velocity

	outputs:
	float. Final position of projectile at time t
	"""

	return -0.5 * 9.81 * t**2 + v_init * t


def s_sim_multiple(t, v_init, s_init, delta_t):
	"""returns estimations of projectile's position up until a final time t.

	inputs:
	t - float. Final time
	v_init - float. Initial velocity
	s_init - float. Initial position
	delta_t - float. Time interval for estimations

	outputs:
	positions - list of floats. All positions from 0 to t in 1s intervals.
	"""

	t_current = 0
	v_current = v_init
	s_current = s_init

	positions = [s_init]

	# to solve this problem, 1-second intervals must be used; thus, delta_t <= 1
	if delta_t > 1:
		delta_t = 1
		spacing = 1
	else:
		spacing = int(1/delta_t)


	while t_current < t and s_current >= 0:
		s_current = s_next(v_current, s_current, delta_t)

		positions.append(s_current)

		v_current = v_next(v_current, s_current, delta_t)
		t_current += delta_t

	# because spacing is 1/delta_t, this returns height at all times t where
	# t is an integer
	return positions[::spacing]
