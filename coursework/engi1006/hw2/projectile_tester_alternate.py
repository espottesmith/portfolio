#==============================================================================
# HW 2 - problem 2
#
# Evan Spotte-Smith
# 09/28/15
# File: projectile_tester_alternate.py
#
# This file reimplements the algorithm of projectile_tester using a more
# succinct function from projectile.py
#==============================================================================


import projectile


def main():
    """tests projectile.py s_sim_multiple algorithm

    inputs: None

    outputs: None

    """

    # set up intitial values
    v_0 = 330
    s_0 = 0
    t=0
    delta_t = .05

    s=s_0 #start s off at s_0

    # print a table with values computed both ways for positive positions

    print('seconds \t distance_sim \t \t distance_formula')
    print('-----------------------------------------------------------')

    # there is no harm in giving arbitrarily large values of t
    # the program halts and returns the positions once s < 0
    s_sim_results = projectile.s_sim_multiple(10000, v_0, s_0, delta_t)

    for result in s_sim_results:
        s_formula = projectile.s_standard(t,v_0)
        print( '{:>5d} \t \t {:.5f} \t \t {:.5f}'.format(t,result,s_formula))
        t += 1


main()
