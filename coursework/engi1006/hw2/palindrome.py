# =============================================================================
# HW 2 - problem 1
#
# Evan Spotte-Smith
# 09/28/15
# File: palindrome.py
#
# This program contains a function to verify if a string is palindromic
# =============================================================================


def is_palindrome(string):
    """compares elements of an str to determine if elements are the same
        backwards and forwards.

    inputs:
    string - str

    outputs:
    bool
    """

    # establish list of characters yet to be compared
    todo = string


    while len(todo) > 0:
        # compare first and last characters
        if todo[0] != todo[-1]:
            return False

        # remove todo's first and last characters, repeat the process
        todo = todo[1:-1]

    # if len(todo) == 0, then no comparison has failed; string is palindromic
    return True
