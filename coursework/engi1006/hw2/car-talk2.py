# =============================================================================
# HW 2 - problem 1
#
# Evan Spotte-Smith
# 09/28/15
# File: car-talk.py
#
# This program answers a "Car Talk" logic puzzle
# It moves through all 6-digit numbers to find a solution fulfilling certain
# requirements.
# =============================================================================

# the main program body is organized to check these conditions
# stage 1: number should have a palindrome in the last 4 (not 5) digits
# stage 2: number + 1 should have a palindrome in the last 5
# stage 3: number + 2 should have a palindrome in the middle 4 digits
# stage 4: number + 3 should have a palindrome in all 6 digits


from palindrome import is_palindrome


def pad(num):
    """formats string with leading 0's.

    inputs:
    num - int

    outputs:
    padded_str - str
    """

    # only numbers with less than 6 digits require padding
    if num < 100000:
        string = str(num)
        padded_str = '0'*(6 - len(string)) + string
        return padded_str

    else:
        return str(num)


def main():
    """searches for Car Talk puzzle answer

    inputs: None

    outputs: None
    """

    for n in range(0, 999999):
        # these checks ensure that the numbers are repsesented as they would be
        # on the odometer
        stage_one = pad(n)

        if is_palindrome(stage_one[2:]) and not is_palindrome(stage_one[1:]):
            stage_two = pad(n+1)

            if is_palindrome(stage_two[1:]):
                stage_three = pad(n+2)

                if is_palindrome(stage_three[1:-1]):
                    stage_four = pad(n+3)

                    if is_palindrome(stage_four):
                        print("{0} is the number!".format(n))
                        break


main()
