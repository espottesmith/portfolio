Car Talk: Homework Two Problem 1
================================

Who wrote it?
-------------
Evan Spotte-Smith
ews2122

What is it?
-----------
This program encodes a solution to a work problem featured on the public radio
show "Car Talk". The problem asks for a 6-digit whole number n (leading 0's
included) where:

    - the last 4 digits of n are palindromic, but not the last 5
    - the last 5 digits of (n + 1) are palindromic
    - the middle 4 digits of (n + 2) are palindromic
    - all 6 digits of (n + 3) are palindromic

What does it contain?
---------------------
Two files in this directory contain programs relating to the "Car Talk" problem:
car-talk2.py and palindrome.py.

What does it do?
----------------
palindrome.py contains a single predicate function, is_palindrome, which takes a
sequence type (in this case, a string) and iteratively works through the list to
determine if it is a palindrome; that is, is the string the same forwards as it
is backwards? car-talk2.py contains two functions, pad and main. The pad
function converts numbers lower than 100000 to strings with trailing 0s. For
instance, 2 would become "000002". The main function uses pad and is_palindrome
to check through all integers less than 1000000 (all integers that can be
represented as strings of length 6), printing a success message upon finding
such a number.

How was it designed?
--------------------
The goal for this project was modularity. To that effect, the behavior of the
program was encapsulated in three functions, rather than one in the previous
implementation. The separation of functions was determined by project guidelines
- pad and is_palindrome could have been placed in a single utility file, with
main separated, but because the project description called for the file with
is_palindrome to be named palindrome.py, this structure no longer made sense.

In terms of individual function implementation, the function that left the most
choices was is_palindrome. There are three obvious ways to implement this - a
method using Python slices, an iterative method, and a recursive method. The
iterative method, using a while-loop, was chosen because of readability.
Although a[::-1] might make sense to seasoned pythonistas, it is less readable
to users of other languages while offering no real benefit in terms of
efficiency. Recursion is less readable in Python than in more functional
languages, so it is often not advisable; again, the recursive solution does not
offer a significant benefit in performance.

How does one use it?
--------------------
Run:
    python car-talk2.py
in a terminal window. This will call main in car-talk2.py. The answer should
appear in the terminal in a second or so - if it does not, cancel program
execution.
