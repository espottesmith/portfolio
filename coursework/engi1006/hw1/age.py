# =================================================================================================
# HW 1 - problem 1
#
# Evan Spotte-Smith
# 09/14/15
# File: age.py
#
# This program calculates someone's age in days based on their date of birth
# =================================================================================================

import datetime as dt

# to allow flexibility, users can include the month in string or number format
MONTHS_NUM = {
    'January':1,
    'February':2,
    'March':3,
    'April':4,
    'May':5,
    'June':6,
    'July':7,
    'August':8,
    'September':9,
    'October':10,
    'November':11,
    'December':12
}

# a check for February is included later - it is not always 28 days, obviously
MONTHS_DAYS = {
    1:31,
    2:28,
    3:31,
    4:30,
    5:31,
    6:30,
    7:31,
    8:31,
    9:30,
    10:31,
    11:30,
    12:31
}

try:
    year = int(input("In what year were you born? "))
    # not an int because string input will be allowed
    month = input("In what month were you born? ")
    day = int(input("On what day of the month were you born? "))

    today = dt.date.today()

    # year sanity check
    if year > today.year:
        raise ValueError()

    # month sanity check
    try:
        month = int(month)

    # if the string value of the month is used
    except ValueError:
        if MONTHS_NUM[month]:
            month = MONTHS_NUM[month]
        else:
            print("""You did not include a valid month! Use the full month name (capitalized) or the
                    month's number in the year.""")

    # day sanity check
    if (day < 0) or (day > MONTHS_DAYS[month]):
        # February is difficult
        # checks for days based on leap year and normal years
        if month == 2:
            if year % 4 == 0:
                if day > 29:
                    raise ValueError()
            else:
                raise ValueError()
        else:
            raise ValueError()

    dob = dt.date(year, month, day)

    hundred_ten_years_ago = dt.date((today.year - 110), today.month, today.day)

    # today - dob will return a datetime.timedelta object with the difference between the two dates
    diff = today - dob

    hundred_ten_years = today - hundred_ten_years_ago

    if diff.days > hundred_ten_years.days:
        print("You are too old!")
    else:
        print(diff.days)

except:
    print("You did not include a valid date of birth!")
