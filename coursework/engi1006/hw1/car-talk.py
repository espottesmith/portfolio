# =================================================================================================
# HW 1 - problem 2
#
# Evan Spotte-Smith
# 09/14/15
# File: car-talk.py
#
# This program answers a "Car Talk" logic puzzle
# It moves through all 6-digit numbers to find a solution fulfilling certain requirements.
# =================================================================================================

# the main program body is organized to check these conditions
# stage 1: number should initially have a palindrome in the last four digits, but not five
# stage 2: number + 1 should have a palindrome in the last five
# stage 3: number + 2 should have a palindrome in the middle 4 digits
# stage 4: number + 3 should have a palindrome in all 6 digits

def is_palindrome(string):
    """
    compares elements of an str to determine if elements are the same backwards and forwards.

    inputs:
    string - str

    outputs:
    bool
    """

    todo = string
    while len(todo) > 0:
        if todo[0] != todo[-1]:
            return False

        todo = todo[1:-1]

    return True


for n in range(0, 999999):
    stage_one = str(n)

    # these checks ensure that the numbers are repsesented as they would be on the odometer
    if n < 100000:
        stage_one = '0'*(6 - len(stage_one)) + stage_one

    if is_palindrome(stage_one[2:]) and not is_palindrome(stage_one[1:]):

        stage_two = str(n+1)

        if n+1 < 100000:
            stage_two = '0'*(6 - len(stage_two)) + stage_two

        if is_palindrome(stage_two[1:]):

            stage_three = str(n+2)

            if n+2 < 100000:
                stage_three = '0'*(6 - len(stage_three)) + stage_three

            if is_palindrome(stage_three[1:-1]):

                stage_four = str(n+3)

                if n+3 < 100000:
                    stage_four = '0'*(6 - len(stage_four)) + stage_four

                if is_palindrome(stage_four):

                    print("{0} is the number!".format(n))
