# =================================================================================================
# HW 1 - problem 3
#
# Evan Spotte-Smith
# 09/14/15
# File: weird.py
#
# This program implements "Russian Peasant" multiplication
# =================================================================================================

def russian_peasant(num_one, num_two):
    """
    implements Russian Peasant multiplication method:
        - given two integers, a and b, continuously multiply a by 2 and integer-divide b by 2
        - at each iteration, if b is odd, add a to the total
        - stop when b is 0

    inputs:
    num_one: int
    num_two: int

    outputs:
    total: int
    """
    total = 0
    while num_two > 0:
        if num_two % 2 == 1:
            total += num_one

        num_one *= 2
        num_two = num_two // 2

    return total

response = ""
while response.lower() != "quit":
    num_one = int(input("What is the first integer to be multiplied? "))
    num_two = int(input("What is the second integer to be multiplied? "))

    print("Using Russian Peasant: {0} * {1} = {2}".format(num_one, num_two, russian_peasant(num_one, num_two)))
    response = input("Enter 'quit' to stop: ")
