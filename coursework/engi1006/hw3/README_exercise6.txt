Homework Three: Chapter 5 Exercise 6
====================================

Who wrote it?
-------------
Evan Spotte-Smith
ews2122

What is it?
-----------
This program is a demonstration of simple file manipulation in Python. Using
builtin IO capabilities with file objects, one file is read, and its contents
are fed into another file so that every other line in the first file appears
in the second.

What does it contain?
---------------------
Exercise 6 is a single-file module which is held in exercise6.py.

What does it do?
----------------
exercise6.py contains two functions, every_other_line and main.
every_other_line contains the bulk of the solution. The every_other_line
function reads the contents of an input file as a list and then slices that
list to produce a list of every other line (the first, third, fifth, etc.).
These lines are then written into an output file with file.write. If the user
specifies a particular path for the output file, every_other_line uses the
os.chdir function to move to that directory and execute the writing there
instead of the original working directory.

The main function simply uses the generalized every_other_line function to
write every other line from "thisFile.txt" to "thatFile.txt".

How was it designed?
--------------------
The main concern in designing this module was to allow flexibility while also
ensuring that the program performed as intended. In every_other_line, the
user is expected to give strings as arguments for infile and outfile. The
expectation, based on how the file was designed with the path kwarg, is that
the user will give file names only, and not use paths. However,
users may choose to eschew the path kwarg and use a relative or absolute path
for outfile, or even for infile. While every_other_line could have parsed
infile and outile to remove any paths included, this approach was not
ultimately taken. This function is not meant to be used with paths, but no
error will arise from their use, and thus, they are permitted.

How does one use it?
--------------------
To run with default parameters, use
    python exercise6.py
To output the contents of thisFile to a location in another directory,
change the path argument in the every_other_line call of main (line 63).
By default, the previous directory will be used.
