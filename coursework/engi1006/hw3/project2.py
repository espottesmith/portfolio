# =============================================================================
# HW 3 - problem 2
#
# Evan Spotte-Smith
# 10/12/15
# File: project2.py
#
# This file implements str.find()-like behavior, for single-instance and
# multi-instance searching.
# =============================================================================

def sanitize(string, substring, start, end):
    """sanitize inputs for find and multi_find functions.

    inputs:
    string - str.
    substring - str.
    start - int. The index at which to begin the search
    end - int. The index at which to end the search

    outputs:
    sanitized_inputs - tuple. This will contain sanitized versions of start and
        end based on the lengths of string and substring.

    """

    use_start = start
    use_end = end

    # difference in length determines where to stop the search
    len_diff = len(string) - len(substring)

    # if user provides invalid indices, search the whole string
    if start > end:
        use_start = 0
        use_end = len_diff

    if start < 0:
        use_start = 0
    elif start > len_diff:
        use_start = len_diff

    # the user cannot take the search past where it is possible to find match
    if end > len_diff:
        use_end = len_diff

    return (use_start, use_end)


def find(string, substring, start, end):
    """re-implements the str.find() method.

    inputs:
    string - str. The string to be searched
    substring - str.
    start - int. The index at which to begin the search
    end - int. The index at which to end the search

    outputs:
    int. The index where the first character of the substring exists
        in string. returns -1 if no match has been found
    """

    # difference in length determines where to stop the search
    len_diff = len(string) - len(substring)

    # check edge cases

    # the substring can never be longer than the string to be searched
    if len_diff < 0:
        return -1
    # if the lengths are identical, the strings must be identical, or not match
    elif len_diff == 0:
        if string == substring:
            return 0
        else:
            return -1

    # ensure that start and end indices are valid
    use_start, use_end = sanitize(string, substring, start, end)

    for i in range(use_start, use_end):
        if string[i:(i + len(substring))] == substring:
            return i

    # if no match has been found within the for-loop
    return -1


def multi_find(string, substring, start, end):
    """re-implements the str.find() method.

    inputs:
    string - str. The string to be searched
    substring - str.
    start - int. The index at which to begin the search
    end - int. The index at which to end the search

    outputs:
    indices - str. A comma-separated string of indices found using the above
        find method. indices = "" if no match has been found
    """

    indices = ""

    # difference in length determines where to stop the search
    len_diff = len(string) - len(substring)

    # check edge cases

    # the substring can never be longer than the string to be searched
    if len_diff < 0:
        return -1
    # if the lengths are identical, the strings must be identical, or not match
    elif len_diff == 0:
        if string == substring:
            return 0
        else:
            return -1

    use_start, use_end = sanitize(string, substring, start, end)

    current_index = use_start

    while current_index < use_end:
        next_index = find(string, substring, current_index, use_end)

        if next_index == -1:
            return indices.rstrip(",")
        else:
            indices += str(next_index) + ","
            current_index = next_index + 1
