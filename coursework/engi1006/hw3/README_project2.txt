Homework 3: Chapter 6 Project 2
===============================

Who wrote it?
-------------
Evan Spotte-Smith
ews2122

What is it?
-----------
This program re-implements the str.find() method in a functional form. It also
extends this method with a function to find all match indices, not only the
first (as is the case with str.find()).

What does it contain?
---------------------
This project is contained in a single file: project2.py.

What does it do?
----------------
project2.py contains three files. Two are the find and multi_find functions.
find performs a basic analysis of the string and substring to be searched;
if it is impossible for a match to occur (because the substring is longer than
the string, etc.), -1 (failure) is returned without any search being conducted.
If the string and substring are an exact match, 0 is returned without a search,
reflecting that the match begins at the first character. For all other cases,
the function moves from the start index to the end index, checking sets of
characters with length equal to the substring length for a match. If a match
arises, the loop ends and that index is returned. multi_find works similarly -
after eliminating edge cases, multi_find uses find iteratively in the
appropriate range of indices until the range is exhausted or find returns -1.

Both find and multi_find rely on a helper function, sanitize, which ensures
that the start and end indices given to find and multi_find are logical.
For instance, if the end of the range is higher than the beginning, the start
index is set to 0 and the end is set to the highest value where a match can be
found.

How was it designed?
--------------------
These functions were designed to reduce code reuse. Originally, in both
functions, the start and end parameters had to be sanity checked. Seeing as
this was a common operation, that sanitation behavior was encapsulated in a
new function, sanitize. Obviously, multi_find was written to use find, thus
preventing that code from having to be rewritten. The edge case checks common
to both find and multi_find were originally in a separate helper function, but
this function barely reduced the lines of code in both functions, so
ultimately, these checks were returned to find and multi_find to increase
readability.

How does one use it?
--------------------
This module does not have a main function, so it cannot be used in a standalone
fashion. However, if it is desirable to use find and multi_find instead of
the builtin str.find() method, one can import this behavior like so:
    import project2
or
    from project2 import find, multi_find
