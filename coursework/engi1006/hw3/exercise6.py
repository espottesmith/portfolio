# =============================================================================
# HW 3 - problem 1
#
# Evan Spotte-Smith
# 10/12/15
# File: exercise6.py
#
# This program demonstrates knowledge of file IO by reading and writing text
# files and maneuvering them using the os stdlib module.
# =============================================================================


import os


def every_other_line(infile, outfile, path=None):
    """move half of contents of one file to another.

    inputs:
    infile - string. infile should be a valid file name
    outfile - string. outfile should be a text file name, but need not already
        exist.
    path - string, or None. Users should use file names (not paths) for infile
        and outfile. In this case, path should be a valid path (relative or
        absolute).

    """

    try:

        this_file = open(infile, "r")
        other_lines =  []

        index = 0
        for line in this_file:
            if index % 2 == 0:
                other_lines.append(line)
            index += 1

        if path != None:
            os.chdir(path)

        that_file = open(outfile, "w")

        for line in other_lines:
            that_file.write(line)

        that_file.close()
        this_file.close()

    except FileNotFoundError:
        print("Invalid file name. Please try a valid file name.")

    except OSError:
        print("Invalid file or path. Please try string paths or file names.")


def main():
    """uses every_other_line to transfer half of the contents of thisFile.txt
        to thatFile.txt.

    inputs: None

    outputs: None

    """

    path = input("Output to (enter path; leave blank for current directory): ")

    if path:
        # NOTE: this assumes you are using a UNIX-like OS (Linux, BSD, OSX)
        every_other_line("thisFile.txt", "thatFile.txt", path=path)
    else:
        every_other_line("thisFile.txt", "thatFile.txt")

main()
