Homework 4
==========

Who wrote it?
-------------
Evan Spotte-Smith
ews2122

What is it?
-----------
These programs implement some basic features of image manipulation for PPM
files using file IO and list manipulations. Specifically, filtering,
color manipulation (grayscale and color-negation), and horizontal mirroring
were all implemented.

What does it contain?
---------------------
Two files are contained in this project: effects.py and effects_tester.py.

What does it do?
----------------
effects.py is a module that holds all of the image manipulation functions,
as well as a host of helper functions. The primary interface with which other
programs will use effects.py is apply_effects, which takes as input file names
for input and output, as well as a Boolean list detailing which effects should
be applied. It maintains temporary output files and sequentially applies all
indicated effects before placing the final result in the desired output file.
All other functions use grab_parameters, which reads the first three lines of
the PPM file and returns the values as a parameter dict, and parse_ppm, which
creates a new text file with the PPM values in a regular matrix, with rows
and columns of length indicated by the PPM parameters. parse_ppm makes matrix
transforms much easier, especially for mirror, though it is a costly operation.

The actual PPM manipulation functions are object_filter, shades_of_gray,
negate_red, negate_blue, negate_green, and mirror. Object filter is the only
function that takes a list of file names - it moves pixel-by-pixel (and even
value-by-value) to find the average value of the pixels, filtering out added
objects. It works with the build_consensus helper function, which compares
various basic statistical values (mean, median, and mode) to find the average
value. shades_of_gray takes the average of the rgb values for each pixel and
sets each of the rbg values to that average, producing a shade of gray. This
produces what is commonly known as a grayscale image. negate_red, negate_green,
and negate_blue all work similarly - for one of the rgb values, change the
value to the maximum - the current value. This is a negation because it changes
the value to the furthest value away within the acceptable range for the PPM
file. Finally, mirror performs a row-by-row transform, flipping the position
of each pixel without flipping the rgb values within each pixel.

effects_tester implements a command-line interface (CLI) with which to use
effects.py. The CLI allows users to include as many files as they would like
(though if object_filter is not called, only the first will be used),
as well as what transforms they want. effects_tester.py then calls
effects.apply_effects based on this input.

How was it designed?
--------------------
effects and effects_tester were designed for modularity and reusability. As
usual, helper functions were added to effects.py to reduce code reuse, but also
because these helper functions could later be applied to other uses; it would
not be difficult to convert parse_ppm to work with some other file
specification or to produce a different format (CSV, TSV, etc.). Though the
initial assignment said to ask the user for their input, I thought a CLI would
be far more useful. If this was to become a real program, I would not want to
have to enter input line-by-line; I would want to be able to enter everything
at once and let the program run, or better yet, write other shell scripts to
run the program.

Performance was a heated topic in designing this module. Previously, it was
expressed that programs written to perform file IO should reduce their memory
footprint. This meant that holding intermediate matrices (producing a list of
list with parse_ppm, rather than a new text file) would not be desired.
However, for the desired design, these intermediate states were essential, so
file IO had to be used. These file manipulations are extremely costly and
increase the runtime substantially. The more time-efficient solution would be
to use lists, as mentioned previously, or even better, to use a dedicated
matrix library like numpy.

How does one use it?
--------------------
Try running it in the command-line! Specify each function you want to use using
the following flags:
    --filter <- object_filter
    --grayscale <- shades_of_gray
    --negate-red <- negate_red
    --negate-green <- negate_green
    --negate-blue <- negate_blue
    --mirror <- mirror

For instance, if you want to use an object filter, negate red, and mirror the
image:

    ./effects_tester.py -i tetons1.ppm tetons2.ppm tetons3.ppm -o tetons.ppm --filter --negate-red --mirror

If you get an error stating that you do not have permission to run the command,
first execute the following line:

    sudo chmod +x effects_tester.py

Note: you need root privileges to execute the above
