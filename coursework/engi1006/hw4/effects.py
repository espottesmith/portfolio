# =============================================================================
# HW 4
#
# Evan Spotte-Smith
# 10/27/15
# File: effects.py
#
# This file implements a number of photo manipulation effects, as in a program
# like GIMP or Photoshop.
# =============================================================================


import os
import statistics as stats


def grab_parameters(in_file):
    """returns first three rows of PPM file in the form of a dict.

    inputs:
    in_file - file object.

    outputs:
    params - dict.

    """

    params = {}

    # these lines assume that all PPM files follow the same format:
    # first line should be the magic number
    # second line should be two numbers, rows and columns
    # third line should be the maximum value
    magic = in_file.readline().rstrip()
    cols, rows = in_file.readline().rstrip().split()
    maximum = in_file.readline().rstrip()

    try:
        params = {
                    'magic':magic,
                    'rows': int(rows),
                    'cols': int(cols),
                    'maximum': int(maximum),
                    }

    except ValueError:
        print("PPM file format invalid. Please check your file header.")

    finally:
        return params


def parse_ppm(in_file, out_file_name, params=None):
    """rewrites PPM file with rows and columns fitting its parameters

    inputs:
    in_file - file object.
    out_file_name - str. out_file_name should be a path to put the parsed/rewritten
        PPM file.
    params - dict, or None.

    outputs: None.

    """

    out_file = open(out_file_name, 'w')
    numbers = []

    if params:
        magic = params['magic']
        rows = params['rows']
        cols = params['cols']
        maximum = params['maximum']
    else:
        # these lines assume that all PPM files follow the same format:
        # first line should be the magic number
        # second line should be two numbers, rows and columns
        # third line should be the maximum value
        magic = in_file.readline().rstrip()
        cols, rows = in_file.readline().rstrip().split()
        maximum = in_file.readline().rstrip()

    try:
        rows = int(rows)
        cols = int(cols)

        # this does not include the header
        for line in in_file:
            numbers += line.rstrip().split(" ")

        # transform PPM file into appropriate rows-by-columns matrix
        matrix = []
        index = 0
        while index < rows * cols * 3:
            matrix.append(numbers[index : index + cols * 3])
            index += cols * 3

        for row in matrix:
            out_file.write(" ".join(row) + "\n")

    except ValueError:
        print("PPM file format invalid. Please check your file header.")

    finally:
        out_file.close()


def build_consensus(data):
    """apply basic statistical methods to deduce the correct pixel value.

    inputs:
    data - list of ints.

    outputs:
    result - int.

    """

    mode = stats.mode(data)
    median = int(stats.median(data))
    mean = stats.mean(data)

    # if the mode represents a majority
    if data.count(mode) > int(len(data) / 2) or mode == median:
        return mode
    else:
        return int(mean)


def apply_effects(in_file_name, out_file_name, effects, *filter_files):
    """primary function that is called by the effects_tester.

    inputs:
    in_file_name - str. the name the primary input file
    out_file_name - str. the name of the output file
    effects - list of booleans. Each position indicates whether we want
        that effect activated
        effects[0] object_filter
        effects[1] shades_of_gray
        effects[2] indicates negate_red
        effects[3] indicates negate_green
        effects[4] indicates negate_blue
        effects[5] indicates mirror
	*filter_files - list of strings. Stores the names of additional files for
        the object_filter

    outputs: None.

	"""

    in_stream = None
    out_stream = None

    # temporary files for intermediary steps
    tmp_file_name = "apply_effects.ppm"
    secondary = "tmp.ppm"

    if effects[0]:
        all_files_names = [in_file_name] + list(filter_files)
        all_files = [open(f, 'r') for f in all_files_names]
        out_stream = open(tmp_file_name, 'w')

        object_filter(all_files, out_stream)

        for stream in all_files:
            stream.close()

        out_stream.close()


    if effects[1]:
        if out_stream is not None:
            os.rename(tmp_file_name, secondary)

            in_stream = open(secondary, 'r')
            out_stream = open(tmp_file_name, 'w')

        else:
            in_stream = open(in_file_name, 'r')
            out_stream = open(tmp_file_name, 'w')

        shades_of_gray(in_stream, out_stream)

        in_stream.close()
        out_stream.close()

    if effects[2]:
        if out_stream is not None:
            os.rename(tmp_file_name, secondary)

            in_stream = open(secondary, 'r')
            out_stream = open(tmp_file_name, 'w')

        else:
            in_stream = open(in_file_name, 'r')
            out_stream = open(tmp_file_name, 'w')

        negate_red(in_stream, out_stream)

        in_stream.close()
        out_stream.close()

    if effects[3]:
        if out_stream is not None:
            os.rename(tmp_file_name, secondary)

            in_stream = open(secondary, 'r')
            out_stream = open(tmp_file_name, 'w')

        else:
            in_stream = open(in_file_name, 'r')
            out_stream = open(tmp_file_name, 'w')

        negate_green(in_stream, out_stream)

        in_stream.close()
        out_stream.close()

    if effects[4]:
        if out_stream is not None:
            os.rename(tmp_file_name, secondary)

            in_stream = open(secondary, 'r')
            out_stream = open(tmp_file_name, 'w')

        else:
            in_stream = open(in_file_name, 'r')
            out_stream = open(tmp_file_name, 'w')

        negate_blue(in_stream, out_stream)

        in_stream.close()
        out_stream.close()

    if effects[5]:
        if out_stream is not None:
            os.rename(tmp_file_name, secondary)

            in_stream = open(secondary, 'r')
            out_stream = open(tmp_file_name, 'w')

        else:
            in_stream = open(in_file_name, 'r')
            out_stream = open(tmp_file_name, 'w')

        mirror(in_stream, out_stream)

        in_stream.close()
        out_stream.close()

    # if some effect was applied, make sure out_stream is the output file
    if sum(effects) > 0:
        os.rename(tmp_file_name, out_file_name)
    # if secondary had to be used, remove the temporary file
    if sum(effects) > 1:
        os.remove(secondary)


def object_filter(in_file_list, out_file):
    """filters out pixel values that appear in only a minority
        of the images in the parameter in_file_list

    inputs:
    in_file_list - list of strings.
    out_file - file object.

    outputs: None.

    """

    params = [grab_parameters(f) for f in in_file_list]
    # have to do this because cannot do set comprehensions with dicts
    all_equal = True
    for param in params:
        if param != params[0]:
            all_equal = False

    # if all files can be compared
    if all_equal:
        # return the single dict in params
        params = params[0]

        # write header to output file
        out_file.write(params['magic'] + "\n")
        out_file.write(str(params['cols']) + " " + str(params['rows']) + "\n")
        out_file.write(str(params['maximum']) + "\n")

        tmp_files = []

        a = 1
        for stream in in_file_list:
            file_name = str(a) + ".ppm"
            # create temporary, reorganized ppm file
            tmp_files.append("/tmp/" + file_name)
            parse_ppm(stream, "/tmp/" + file_name, params=params)
            a += 1

        tmp_streams = [open(f, 'r') for f in tmp_files]

        for i in range(0, params['rows']):
            # list of each file's version of this the current row
            this_row = []
            # final version - after consensus-building
            result_row = []

            for stream in tmp_streams:
                one_row = stream.readline().rstrip().split()
                one_row = [int(a) for a in one_row if a != '']
                this_row.append(one_row)

            for j in range(0, params['cols'] * 3):
                # as with this_row, each file's version of this column
                # NOTE: this will run separately for r, g, and b
                this_col = []

                for row in this_row:
                    this_col.append(row[j])

                result_value = build_consensus(this_col)
                result_row.append(result_value)

            result_row_str = [str(a) for a in result_row]
            out_file.write(" ".join(result_row_str) + "\n")

        for stream in tmp_streams:
            stream.close()

        for path in tmp_files:
            os.remove(path)

    else:
        print("PPM Files cannot be compared. Please check your file headers.")



def shades_of_gray(in_file, out_file):
    """converts the color image in_file to black and white

    inputs:
    in_file - file object.
    out_file - file object.

    outputs: None.

    """

    params = grab_parameters(in_file)

    # write header to output file
    out_file.write(params['magic'] + "\n")
    out_file.write(str(params['cols']) + " " + str(params['rows']) + "\n")
    out_file.write(str(params['maximum']) + "\n")

    tmp_file_name = "/tmp/shades_of_gray.ppm"
    parse_ppm(in_file, tmp_file_name, params=params)

    tmp = open(tmp_file_name, 'r')

    for i in range(0, params['rows']):
        this_row = tmp.readline().rstrip().split()
        this_row = [int(a) for a in this_row if a != '']
        result_row = []

        for j in range(0, params['cols'] * 3, 3):
            rgb = [this_row[j], this_row[j+1], this_row[j+2]]
            # in grayscale, pixel value is the average of r, g, and b
            mean = int(stats.mean(rgb))
            rgb = [mean, mean, mean]
            result_row += rgb

        result_row_str = [str(a) for a in result_row]
        out_file.write(" ".join(result_row_str) + "\n")

    tmp.close()
    os.remove(tmp_file_name)


def negate_red(in_file, out_file):
    """negates the red in an image

    inputs:
    in_file - file object.
    out_file - file object.

    outputs: None.

    """

    params = grab_parameters(in_file)

    # write header to output file
    out_file.write(params['magic'] + "\n")
    out_file.write(str(params['cols']) + " " + str(params['rows']) + "\n")
    out_file.write(str(params['maximum']) + "\n")

    tmp_file_name = "/tmp/negate_red.ppm"
    parse_ppm(in_file, tmp_file_name, params=params)

    tmp = open(tmp_file_name, 'r')

    for i in range(0, params['rows']):
        this_row = tmp.readline().rstrip().split()
        this_row = [int(a) for a in this_row if a != '']
        result_row = []

        for j in range(0, params['cols'] * 3, 3):
            # negate red => make red-value maximum - red
            rgb = [params['maximum']-this_row[j], this_row[j+1], this_row[j+2]]
            result_row += rgb

        result_row_str = [str(a) for a in result_row]
        out_file.write(" ".join(result_row_str) + "\n")

    tmp.close()
    os.remove(tmp_file_name)


def negate_green(in_file, out_file):
    """negates the green in an image

    inputs:
    in_file - file object.
    out_file - file object.

    outputs: None.

    """

    params = grab_parameters(in_file)

    # write header to output file
    out_file.write(params['magic'] + "\n")
    out_file.write(str(params['cols']) + " " + str(params['rows']) + "\n")
    out_file.write(str(params['maximum']) + "\n")

    tmp_file_name = "/tmp/negate_green.ppm"
    parse_ppm(in_file, tmp_file_name, params=params)

    tmp = open(tmp_file_name, 'r')

    for i in range(0, params['rows']):
        this_row = tmp.readline().rstrip().split()
        this_row = [int(a) for a in this_row if a != '']
        result_row = []

        for j in range(0, params['cols'] * 3, 3):
            # negate green => make green-value maximum - green
            rgb = [this_row[j], params['maximum']-this_row[j+1], this_row[j+2]]
            result_row += rgb

        result_row_str = [str(a) for a in result_row]
        out_file.write(" ".join(result_row_str) + "\n")

    tmp.close()
    os.remove(tmp_file_name)


def negate_blue(in_file, out_file):
    """negates the blue in an image

    inputs:
    in_file - file object.
    out_file - file object.

    outputs: None.

    """

    params = grab_parameters(in_file)

    # write header to output file
    out_file.write(params['magic'] + "\n")
    out_file.write(str(params['cols']) + " " + str(params['rows']) + "\n")
    out_file.write(str(params['maximum']) + "\n")

    tmp_file_name = "/tmp/negate_blue.ppm"
    parse_ppm(in_file, tmp_file_name, params=params)

    tmp = open(tmp_file_name, 'r')

    for i in range(0, params['rows']):
        this_row = tmp.readline().rstrip().split()
        this_row = [int(a) for a in this_row if a != '']
        result_row = []

        for j in range(0, params['cols'] * 3, 3):
            # negate blue => make blue-value maximum - blue
            rgb = [this_row[j], this_row[j+1], params['maximum']-this_row[j+2]]
            result_row += rgb

        result_row_str = [str(a) for a in result_row]
        out_file.write(" ".join(result_row_str) + "\n")

    tmp.close()
    os.remove(tmp_file_name)


def mirror(in_file, out_file):
    """creates a mirror image by flipping an image horizontally

    inputs:
    in_file - file object.
    out_file - file object.

    outputs: None.

    """

    params = grab_parameters(in_file)

    # write header to output file
    out_file.write(params['magic'] + "\n")
    out_file.write(str(params['cols']) + " " + str(params['rows']) + "\n")
    out_file.write(str(params['maximum']) + "\n")

    tmp_file_name = "/tmp/mirror.ppm"
    parse_ppm(in_file, tmp_file_name, params=params)

    tmp = open(tmp_file_name, 'r')

    for i in range(0, params['rows']):
        this_row = tmp.readline().rstrip().split()
        this_row = [int(a) for a in this_row if a != '']
        result_row = []

        pixels = []

        for j in range(0, params['cols'] * 3, 3):
            # group values by pixel to prevent mixup during mirroring
            pixel = (this_row[j], this_row[j+1], this_row[j+2])
            pixels.append(pixel)

        this_pix = 0
        that_pix = len(pixels) - 1
        # flip pixels on opposite sides
        while this_pix < len(pixels)/2:
            hold = pixels[this_pix]
            pixels[this_pix] = pixels[that_pix]
            pixels[that_pix] = hold

            this_pix += 1
            that_pix -= 1

        for pixel in pixels:
            # degroup => return pixel-tuples to regular values
            result_row += [a for a in pixel]

        result_row_str = [str(a) for a in result_row]
        out_file.write(" ".join(result_row_str) + "\n")

    tmp.close()
    os.remove(tmp_file_name)
