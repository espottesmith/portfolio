#!/usr/bin/env python

# =============================================================================
# HW 4
#
# Evan Spotte-Smith
# 10/27/15
# File: effects_tester.py
#
# This file is a command-line program designed to test effects.py
# =============================================================================


import argparse

import effects


description = "Test effects.py by editing PPM files"

parser = argparse.ArgumentParser(description=description)

parser.add_argument("-i", "--infiles", nargs="+", required=True,
    help="File(s) to be manipulated (only 1 needed except for object_filter)")
parser.add_argument("-o", "--outfile", nargs=1, required=True,
    help="Path to output file. Output file does not need to exist.")
parser.add_argument("--filter", action="store_true",
    help="Call to run object_filter on infiles")
parser.add_argument("--grayscale", action="store_true",
    help="Call to run shades_of_gray on infile")
parser.add_argument("--negate-red", action="store_true",
    help="Call to run negate_red on infile")
parser.add_argument("--negate-green", action="store_true",
    help="Call to run negate_green on infile")
parser.add_argument("--negate-blue", action="store_true",
    help="Call to run negate_blue on infile")
parser.add_argument("--mirror", action="store_true",
    help="Call to run mirror on infile")


if __name__ == "__main__":
    args = parser.parse_args()

    effect_list = [args.filter,
                args.grayscale,
                args.negate_red,
                args.negate_green,
                args.negate_blue,
                args.mirror
                ]

    in_file_name = args.infiles[0]
    filter_files = args.infiles[1:]

    print("Applying effects.")

    effects.apply_effects(in_file_name, args.outfile[0], effect_list,\
        *filter_files)

    print("Done.")
