# =============================================================================
# HW 5
#
# Evan Spotte-Smith
# 11/17/15
# File: percolation.py
#
# This file implements a model for testing the percolation probability of a
# material.
# =============================================================================


import numpy as np
import matplotlib.pyplot as plt


def read_grid(infile_name):
    """create a site vacancy matrix from a text file.

    inputs:
    infile_name - str.

    outputs:
    matrix - np.ndarray. The input file represented as an n-by-n matrix

    """

    try:
        stream = open(infile_name, 'r')
        rows = int(stream.readline().strip())
        cols = rows
        stream.close()
    except IOError:
        print("read_grid failed. Please input a vaild file.")

    matrix = np.loadtxt(infile_name, dtype=int, skiprows=1)
    try:
        # Do not assume that matrix will be written in an n-by-n grid
        matrix.shape = (rows, cols)
    except:
        print("read_grid failed. File did not match expected dimensions.")

    return matrix

def write_grid(outfile_name, sites):
    """write a site vacancy matrix to a file.

    inputs:
    outfile_name - str.
    sites - np.ndarray. This represents a site vacancy matrix

    outputs: None

    """

    stream = open(outfile_name, 'w')

    rows = sites.shape[0]
    stream.write(str(rows) + "\n")

    sites = sites.astype(str)

    for row in sites:
        stream.write(" ".join(row) + "\n")

    stream.close()


def vertical_flow(sites):
    """returns a matrix of vacant/full sites (1=full, 0=vacant)

    inputs:
    sites - np.ndarray.

    outputs:
    flow - np.ndarray. The flow matrix corresponding to the vacancy matrix.

    """

    flow = np.zeros(sites.shape, dtype=int)

    flow[0] = sites[0]

    for i in range(1, len(sites)):
        for j in range(0, len(sites[i])):
            if sites[i][j] == 1 and flow[i-1][j] == 1:
                flow[i][j] = 1
            else:
                flow[i][j] = 0

    return flow


def undirected_flow(sites):
    """returns a matrix of vacant/full sites (1=full, 0=vacant)

    inputs:
    sites - np.ndarray

    outputs:
    flow - np.ndarray. The undirected flow matrix corresponding to sites.

    """

    n = sites.shape[0]
    flow = np.zeros((n, n), dtype=int)

    for i in range(0, n):
        # sometimes, flow will go from one top cell to another
        if sites[0,i] and not flow[0,i]:
            flow_from(sites, flow, 0, i)

    return flow


def flow_from(sites,full,i,j):
    """adjusts the full array for flow from a single site

    inputs:
    sites - np.ndarray
    full - np.ndarray. The equivalent to flow in the above flow functions
    i - int.
    j - int.

    outputs: None

    NOTE: This is an impure function that includes side effects!

    """

    if sites[i, j] and not full[i, j]:
        full[i, j] = 1

        n = sites.shape[0]
        neighbors = []

        if i < (n - 1):
            if sites[i + 1, j] and not full[i + 1, j]:
                # neighbor below
                neighbors.append((i + 1, j))
        if i > 0:
            if sites[i - 1, j] and not full[i - 1, j]:
                # neighbor above
                neighbors.append((i - 1, j))
        if j < (n - 1):
            if sites[i, j + 1] and not full[i, j + 1]:
                # neighbor to the right
                neighbors.append((i, j + 1))
        if j > 0:
            if sites[i, j - 1] and not full[i, j - 1]:
                # neighbor to the left
                neighbors.append((i, j - 1))

        for neighbor in neighbors:
            flow_from(sites, full, neighbor[0], neighbor[1])


def percolates(flow_matrix):
    """returns a boolean if the flow_matrix exhibits percolation

    inputs:
    flow_matrix - np.ndarray

    outputs:
    bool. Does the given flow matrix demonstrate percolation?

    NOTE: This function is not strictly necessary

    """

    return any(flow_matrix[-1])


def make_sites(n,p):
    """returns an nxn site vacancy matrix

    inputs:
    n - int. Number of rows and columns in the matrix to be tested.
    p - float. The site vacancy probability.

    outputs:
    site_matrix - np.ndarray. A randomized matrix with values corresponding to
        the probability p that a given site is vacant.
    """

    randoms = np.random.rand(n, n)
    site_matrix = randoms < p

    return site_matrix.astype(int)


def show_perc(sites):
    """displays a matrix using three colors for vacant, blocked, full

    inputs:
    sites - np.ndarray

    outputs: None

    """

    flow = undirected_flow(sites)

    perc = sites + flow

    fig, ax = plt.subplots()

    # display matrix as an image
    cax = ax.matshow(perc, cmap=plt.cm.RdBu)
    ax.set_title('Percolation')

    cbar = fig.colorbar(cax, ticks=[0, 1, 2])
    cbar.ax.set_yticklabels(["Blocked", "Vacant", "Full"])

    plt.show()


def run_trials(n, p, trials):
    """run a number of percolation trials at a particular vacancy probability.

    inputs:
    n - int. Size of grid to be tested
    p - float. The site vacancy probability
    trials - int.

    outputs:
    prob - float. Average probability of percolation

    """

    successes = 0

    for i in range(trials):
        sites = make_sites(n, p)
        flow = undirected_flow(sites)
        if percolates(flow):
            successes += 1

    return (successes / trials)


def make_plot(n,trials):
    """generates and displays a graph of percolation p vs. vacancy p

    inputs:
    n - int. Size of grid to be tested
    trials - int. Number of trials to be run per data point

    outputs: None

    """

    # smoothness of the graph is dependent on how many trials are run
    # with more trials, we can demand to be more exacting
    if trials >= 9000:
        THRESHOLD = .01
    elif trials >= 5000:
        THRESHOLD = .03
    else:
        THRESHOLD = .05

    all_small = False

    lowest = .01
    highest = .99

    data = []

    data.append((lowest, run_trials(n, lowest, trials)))
    data.append((highest, run_trials(n, highest, trials)))

    while not all_small:
        all_small = True
        for i in range(0, len(data) - 1):
            if abs(data[i+1][1] - data[i][1]) > THRESHOLD:
                all_small = False
                p = (data[i][0] + data[i+1][0])/2
                data.insert((i + 1), (p, run_trials(n, p, trials)))
                break

    site_ps = np.array([datum[0] for datum in data])
    perc_ps = np.array([datum[1] for datum in data])

    plt.plot(site_ps, perc_ps)
    plt.xlabel("Site Vacancy Probability")
    plt.ylabel("Percolation Probability")
    plt.title("Effect of site vacancy probability on likelihood of percolation")
    plt.axis([0, 1, 0, 1])

    plt.show()
