Homework 5
==========

Who wrote it?
-------------
Evan Spotte-Smith
ews2122

What is it?
-----------
This program implements a 2-dimensional undirected percolation model to
determine how site vacancy probability affects the likelihood of percolation.

What does it contain?
---------------------
This program contains two files, percolation.py and hw5_2.py; however, hw5_2.py
is only a pre-written tester file.

What does it do?
----------------
percolation.py includes a number of functions to create percolation
simulations. read_grid and write_grid implement IO for site vacancy matrices,
with read_grid reading true-false values from a text file and importing those
values into a numpy.ndarray and write_grid writing the contents and dimensions
of a numpy.ndarray to a text file.

vertical_flow transforms a site vacancy matrix into a flow matrix for
vertical-only percolation. Assuming that the fluid is flowing from top to
bottom, vertical_flow moves down the matrix, assigning flow to a site if
it is vacant and flow was assigned to the above site. This is a simplification
of flow and percolation that is improved upon by undirected_flow and flow_from.
flow_from is a basic recursive implementation of the Depth-First Search (DFS)
graph searching algorithm. It first determines, based on the starting position
in the graph (matrix), what cells neighbor the starting position; then, it
checks vacancy and flow for those neighbors, calling flow_from on them if they
are vacant but not filled yet. undirected_flow calls flow_from on all vacant,
non-filled cells in the first row of the site matrix to ensure that all
possible means of percolation are investigated.

percolation takes a flow matrix like those created from vertical_flow and
returns a Boolean value - True if flow reached one of the bottom sites, and
False otherwise.

make_sites takes a dimension (only one is needed because all matrices
are square for this simulation) and a site vacancy probability and uses
pseudorandom number generators to create a "random" test matrix. Other
functions used for testing are show_perc, run_trials, and make_plot. show_perc
visualizes flow by adding a site matrix and a flow matrix and displaying the
result with matplotlib.plt.matshow. When the site and flow matrices are summed,
a resulting 0 represents a blocked site, a 1 represents a vacant site, and a
2 represents a full site. run_trials uses make_sites iteratively and returns
the experimental probability of percolation given a particular site vacancy
probability, a matrix size, and a number of trials. Finally, make_plot uses
run_trials to create an adaptive plot of percolation probability against site
vacancy probability. It begins with two endpoints, near 0 and near 1, and then
fills gaps in the data until it generates a smooth curve.

How was it designed?
--------------------
Most of the design for this program centered on efficiency and lowering
runtime. First, flow_from needed to ensure that no work was repeated - that is,
unnecessary function calls had to be eliminated. To do this, there are
consistent checks on edge cases, and the flow matrix is checked at each stage
to ensure that flow_from is not being called on a cell that the flow has
already reached. Another tough design decision came in make_plot. Originally,
it was specified that make_plot should adaptively plot the percolation and
vacancy probabilities. However, in the specification, make_plot was not allowed
to have a threshold variable to determine how smooth the graph needed to be.
So, this was also made roughly adaptive. After extensive testing with the
size of the matrix, the number of trials, and the threshold, 0.01, 0.03, and
0.05 were used; in testing, these numbers, when paired with certain trial
numbers, guaranteed a runtime of less than 10 minutes.

In addition to the concerns for efficiency, there were also some stylistic
choices made in designing percolation. The percolates function was not,
strictly speaking, a necessary function in this module. In the implementation,
it is a short one-liner, using the Python built-in any. This function is still
included because it was part of the original specification for this program.

In read_grid, np.loadtxt was used to generate an array, rather than using
Python file IO. This choice was made for convenience, but it does make the
function somewhat less flexible than it could be - it assumes that the input
text file is already organized in neat rows and columns.

How does one use it?
--------------------
Use the included tester, hw5_2.py. Run:
    python hw5_2.py
in your terminal. Please note that after the percolation visualization is
displayed, you will need to close out of that window before the probability
graph can be displayed.

Note: The user can call percolation.run_trials and percolation.make_plot with
any number of trials per data point. It is recommended that users make the
number of trials 10000, but between 5000 and 7500 should suffice for
reasonable accuracy and precision.
