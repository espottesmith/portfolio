# =============================================================================
# HW 6
#
# Evan Spotte-Smith
# 11/30/15
# File: classifier.py
#
# This file implements the nearest-neighbor classification algorithm with an
# experimental validater.
# =============================================================================

import statistics as stats
import numpy as np
from scipy.spatial import distance


def NNclassifier(training, test):
    """uses the nearest-neighbor algorithm (Fix & Hodges 1952) to classify
        unknown (or "unknown") data points using classified training data

    inputs:
    training - np.ndarray
    test - np.ndarray

    outputs:
    classifications - np.ndarray

    """

    # distance should be calculated without the 0 or 1 in the first column
    nearest_ordering = np.argsort(distance.cdist(test, training[:, 1:]))
    # retain only the closest neighbor
    nearest = nearest_ordering[:,0]

    classifications = np.array([training[n, 0] for n in nearest])

    return classifications


def KNNclassifier(training, test, k_in, dist):
    """uses the k-nearest-neighbor algorithm to classify unknown (or "unknown")
        data points using classified training data and an arbitrary distance
        function

    inputs:
    training - np.ndarray
    test - np.ndarray
    k_in - int. k_in should always be odd to avoid ties between categories.
    dist - str. This should correspond to a valid distance function in
        scipy.spatial.distance.

    outputs:
    classifications - np.ndarray

    """

    if k_in % 2 == 0 and k_in < 22:
        k = k_in + 1
    elif k_in % 2 == 0:
        k = k_in - 1
    elif k_in > 21:
        k = 21
    elif k_in < 1:
        k = 1
    else:
        k = k_in

    # distance should be calculated without the 0 or 1 in the first column
    nearest_ordering = np.argsort(distance.cdist(test, training[:, 1:], dist))
    # retain the k closest neighbors
    nearest_k = nearest_ordering[:,0:k]

    class_list = []
    for row in nearest_k:
        labels = [training[n, 0] for n in row]
        class_list.append(stats.mode(labels))

    classifications = np.array(class_list)

    return classifications


def n_validator(data, p, classifier, *args):
    """estimates performance of a classifier function by using p-fold
        cross-validation

    inputs:
    data - np.ndarray. The data to be used to test the classifier
        (already classified).
    p - int
    classifier - function
    *args - UNKNOWN

    outputs:
    success - float. Success rate average after p-fold cross-validation

    """

    successes = []

    # divide the data into p cells of as close to equal length as possible
    cell = len(data) // p
    # keep track of how many cells get an extra data point
    remainder = len(data) % p

    if cell <= 0:
        raise ValueError(
                "There cannot be {}-fold validation with this data".format(p))


    # data must be randomized to ensure the legitimacy of the cross-validation
    np.random.shuffle(data)

    i = 0
    for j in range(1, p+1):
        this_successes = 0

        if remainder > 0 and remainder >= j:
            test = data[i:i+cell+1, :]
            training = np.delete(data, np.s_[i:i+cell+1], 0)
        else:
            test = data[i:i+cell, :]
            training = np.delete(data, np.s_[i:i+cell], 0)

        # testing data is passed without classification
        classifications = classifier(training, test[:, 1:], *args)

        for k in range(len(classifications)):
            if classifications[k] == test[k, 0]:
                this_successes += 1

        successes.append(this_successes)

        if remainder > 0 and remainder <= j:
            i += (cell + 1)
        else:
            i += cell

    # for average probability over all p cells
    return sum(successes) / len(data)


def experiment_k(data, p, classifier, ks, *args):
    """experimentatlly determine the optimal k (for k-nearest-neighbors) for
        a given data set.

    inputs:
    data - np.ndarray. Already-classified data to be used as a benchmark in
        the experiment.
    p - int
    classifier - function
    ks - list of ints. These should be the various k-values to be tested.
    *args - UNKNOWN

    outputs:
    most_successful - (int, float). Tuple containing the most-successful value
        of k and its probability of success.

    """

    success_rates = []

    for k in ks:
        success_rates.append((k, n_validator(data, p, classifier, k, *args)))

    # sort based on the success rate, in decreasing order (highest rate first)
    sorted_rates = sorted(success_rates, key=(lambda x: x[1]), reverse=True)

    most_successful = sorted_rates[0]

    return most_successful


def experiment_dist(data, p, classifier, ks, dists, *args):
    """experimentally determine the optimal distance function (for
        k-nearest-neighbor) for a given data set.

    inputs:
    data - np.ndarray. Already-classified data to be used as a benchmark in
        the experiment.
    p - int
    classifier - function
    ks - list of ints
    dists - list of strs. These should be the various distance functions
        (specified in scipy.spatial.distance) to be tested.
    *args - UNKNOWN

    outputs:
    results - list of (str, int, float). For each dist, includes the optimal k
        and the success rate for that k.

    """

    results = []

    for dist in dists:
        best = experiment_k(data, p, classifier, ks, dist, *args)
        results.append((dist, best[0], best[1]))

    return results
