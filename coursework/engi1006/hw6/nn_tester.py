#!/usr/bin/env python3

# =============================================================================
# HW 6
#
# Evan Spotte-Smith
# 11/30/15
# File: classifier_tester.py
#
# This file tests classifier.py using real data from the Diagnostic Wisconsin
# Breast Cancer Database as well as simulated normalized data.
# =============================================================================

import statistics as stats
import numpy as np
from nn import n_validator, KNNclassifier, experiment_k, experiment_dist


def make_wisconsin():
    """uses DWBCD data to create a 569-by-31 matrix for testing classifier.py.

    inputs: None

    outputs:
    matrix - np.ndarray

    """

    str_list = []
    with open("wdbc.data", 'r') as wisconsin_file:
        # this may be bad form, but it is roughly equivalent to np.loadtxt
        str_list = wisconsin_file.readlines()

    for i in range(len(str_list)):
        # removes first column (id numbers)
        str_list[i] = str_list[i].split(",")[1:]

    for i in range(len(str_list)):
        # redefine classifiers
        if str_list[i][0].lower() == 'b':
            str_list[i][0] = '0'
        elif str_list[i][0].lower() == 'm':
            str_list[i][0] = '1'

        # eliminate newlines
        str_list[i][-1] = str_list[i][-1].rstrip()

    matrix = np.array(str_list, dtype=float)

    return matrix


def make_simulated(means, covars, points):
    """uses Numpy and Scipy to generate normalized random data sets for
        testing classifier.py.

    inputs:
    means - list of lists. This will be a list of the 2-dimensional means for
        all classes.
    covars - list of lists of lists. This will be a list of 2-by-2 lists,
        one for each class.
    points - list of ints. How many data points each data set will contain.

    outputs:
    matrix - np.ndarray.

    """

    matrix = None

    for i in range(len(means)):
        data = np.random.multivariate_normal(means[i], covars[i], points[i])
        categories = i * np.ones(points[i])
        categories.shape = (points[i], 1)
        # as with make_wisconsin, categories should be on the left
        data = np.hstack((categories, data))

        if matrix is not None:
            matrix = np.vstack((matrix, data))
        else:
            matrix = data

    return matrix


if __name__ == "__main__":

    wis_data = make_wisconsin()

    # based on specifications for randomly generated data
    means = [[2.5, 3.5], [.5, 1]]
    covars = [[[1, 1], [1, 4.5]], [[2, 0], [0, 1]]]
    points = [300, 300]
    sim_data = make_simulated(means, covars, points)

    ks = [i for i in range(1,16,2)]
    # run k experiments on both data sets
    results_wis = []
    results_sim = []
    for i in range(100):
        k_wis = experiment_k(wis_data, 5, KNNclassifier, ks, 'euclidean')
        k_sim = experiment_k(sim_data, 5, KNNclassifier, ks, 'euclidean')

        results_wis.append(k_wis)
        results_sim.append(k_sim)

    try:
        best_k_wis = stats.mode([i[0] for i in results_wis])
        best_succ_wis = stats.mean(
                            [i[1] for i in results_wis if i[0] == best_k_wis])
        best_k_sim = stats.mode([i[0] for i in results_sim])
        best_succ_sim = stats.mean(
                            [i[1] for i in results_sim if i[0] == best_k_sim])
    except stats.StatisticsError:
        # stats.StatisticsError arises from failure of stats.mode
        best_k_wis = stats.median_high([i[0] for i in results_wis])
        best_succ_wis = stats.mean(
                            [i[1] for i in results_wis if i[0] == best_k_wis])
        best_k_sim = stats.median_high([i[0] for i in results_sim])
        best_succ_sim = stats.mean(
                            [i[1] for i in results_sim if i[0] == best_k_sim])

    print("For WBCD data, the ideal k is " + str(best_k_wis) + \
            " with a success rate of " + str(best_succ_wis))
    print("For simulated data, the ideal k is " + str(best_k_sim) + \
            " with a success rate of " + str(best_succ_sim))

    print()

    dists = ['euclidean', 'sqeuclidean', 'cityblock', 'mahalanobis']

    results_wis = []
    results_sim = []

    dist_results_wis = []
    dist_results_sim = []

    for i in range(100):
        ks_wis = experiment_dist(wis_data, 5, KNNclassifier, ks, dists)
        ks_sim = experiment_dist(sim_data, 5, KNNclassifier, ks, dists)

        results_wis.append(ks_wis)
        results_sim.append(ks_sim)

    for j in range(len(dists)):
        try:
            this_dist_wis = results_wis[:][j]
            best_k_wis = stats.mode([i[1] for i in this_dist_wis])
            best_succ_wis = stats.mean(
                                [i[2] for i in this_dist_wis if i[1] == best_k_wis]
                                )
            dist_results_wis.append((dists[j], best_k_wis, best_succ_wis))

            this_dist_sim = results_sim[:][j]
            best_k_sim = stats.mode([i[1] for i in this_dist_sim])
            best_succ_sim = stats.mean(
                                [i[2] for i in this_dist_sim if i[1] == best_k_sim]
                                )
            dist_results_sim.append((dists[j], best_k_sim, best_succ_sim))

        except stats.StatisticsError:
            # stats error could occur if there is no unique mode
            # in this case, use median
            this_dist_wis = results_wis[:][j]
            best_k_wis = stats.median_high([i[1] for i in this_dist_wis])
            best_succ_wis = stats.mean(
                                [i[2] for i in this_dist_wis if i[1] == best_k_wis]
                                )
            dist_results_wis.append((dists[j], best_k_wis, best_succ_wis))

            this_dist_sim = results_sim[:][j]
            best_k_sim = stats.median_high([i[1] for i in this_dist_sim])
            best_succ_sim = stats.mean(
                                [i[2] for i in this_dist_sim if i[1] == best_k_sim]
                                )
            dist_results_sim.append((dists[j], best_k_sim, best_succ_sim))

    for dist, k, success in dist_results_wis:
        print("For WBCD data, with distance metric {}, ".format(dist)
                + "the optimal k is {} ".format(k)
                + "with a success rate of {}.".format(success))

    print()

    for dist, k, success in dist_results_sim:
        print("For simulated data, with distance metric {}, ".format(dist)
                + "the optimal k is {} ".format(k)
                + "with a success rate of {}.".format(success))

    print()

    most_succ_wis = max(dist_results_wis, key=(lambda x: x[2]))
    most_succ_sim = max(dist_results_sim, key=(lambda x: x[2]))

    print("For WBCD data, with optimal k, the best distance metric is "
            + "{} ".format(most_succ_wis[0])
            + "with a success rate of {}.".format(most_succ_wis[2]))

    print("For simulated data, with optimal k, the best distance metric is "
            + "{} ".format(most_succ_sim[0])
            + "with a success rate of {}.".format(most_succ_sim[2]))
